
export const PROFILE_CATEGORY_ADMIN:string = 'ADMIN';
export const PROFILE_CATEGORY_STUDENT:string = 'STUDENT';
export const PROFILE_CATEGORY_SECRETARY:string = 'SECRETARY';

export const TITLE_SUCCESS:string = 'alert.success.title';
export const MESSAGE_OPERATION_SUCCESS:string = 'alert.success.body';

export const TITLE_ERROR:string = 'alert.error.title';
export const MESSAGE_OPERATION_ERROR:string = 'alert.error.body';

export const TITLE_INFORMATION:string = 'alert.information.title';
export const MESSAGE_OPERATION_INFORMATION:string = 'alert.information.body';

export const TITLE_WARNING:string = 'alert.warning.title';
export const MESSAGE_OPERATION_WARNING:string = 'alert.warning.body';


