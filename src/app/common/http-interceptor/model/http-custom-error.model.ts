export class HttpCustomError {
    error?: string;
    message?: string;
    status: number;
    statusText: string;
    url: string;
}
