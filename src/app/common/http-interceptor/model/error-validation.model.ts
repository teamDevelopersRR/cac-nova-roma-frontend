export class ErrorValidation {
    field: string;
    code: string;
    message: string;
    objName: string;

    constructor(field: string, code: string, message: string, objName: string) {
        this.field = field;
        this.code = code;
        this.message = message;
        this.objName = objName;
    }
}