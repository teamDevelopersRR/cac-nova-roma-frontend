import { Router } from '@angular/router';
import { HttpCustomError } from './model/http-custom-error.model';
import { Observable } from 'rxjs/Observable';
import { HttpErrorResponse } from '@angular/common/http'
import { AlertCenterService } from '../shared/components/alert-center/service/alert-center.service';
import { TITLE_WARNING, TITLE_ERROR } from '../../util/ConstantProject';

export class ErrorHandler {

    static handlerError(responseError: HttpErrorResponse | any, alertCenterService: AlertCenterService, router: Router) {
        let error: HttpCustomError = new HttpCustomError();
        if (responseError instanceof HttpErrorResponse) {

            error.url = responseError.url;
            error.status = responseError.status;
            error.statusText = responseError.statusText;

            switch (responseError.status) {
                case 0:
                    error.error = TITLE_ERROR;
                    error.message = "errors.server.serviceUnavailable";
                    alertCenterService.alertDanger(error.error, error.message);
                    break;
                case 401:
                    error.message = "errors.server.loginPasswordInvalid";
                    if(responseError.url.search('authentication') > 0) {
                        alertCenterService.alertWarning(TITLE_WARNING, error.message);
                    } else {
                        router.navigate(['authentication/login']);
                    }
                    break;
                case 403:
                    error.message = "errors.server.notAllowed";
                    alertCenterService.alertDanger(TITLE_ERROR, error.message);
                    break
                case 422: case 500:
                    let errorJSON = responseError.error;
                    error.error = errorJSON.errorType === 'VALIDATION'? errorJSON.fieldErrors : errorJSON.error;
                    error.message = errorJSON.message;
                    break;
                default: break;
            }
        } else {
            error.url = responseError.url ? responseError.url : 'unknown';
            error.status = responseError.status ? responseError.status : 0;
            error.statusText = responseError.statusText ? responseError.statusText : 'unknown';
            error.message = responseError.message ? responseError.message : responseError.toString();
        }
        return Observable.throw(error);
    }
}