import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from './../../../authentication/authentication.service';
import { Menu, ItemsMenu, MainMenuItem } from './menu.model';
import { Injectable } from '@angular/core';

@Injectable()
export class MenuService {

  constructor(private authService: AuthenticationService, private http: HttpClient) { }

  getMenu(): Promise<Menu[]> {
    return this.http.get<Menu[]>('/assets/menu.json').map((data: Menu[]) => {
      let menu = [];
      data.forEach(menuItem => {
        menuItem.childrens = (<MainMenuItem[]>this.childrenFilter(menuItem.childrens));
        if (menuItem.childrens.length > 0) {
          menu.push(menuItem)
        };
      });
      return menu;
    }).toPromise();
  }

  childrenFilter(childremItems: ItemsMenu[]) {
    return childremItems.filter(item => {
      if (!item.permission || this.authService.hasPermission(item.permission)) {
        if (item.type === 'sub') {
          item.childrens = this.childrenFilter(item.childrens);
        }
        return true;
      } else {
        return false;
      }
    });
  }

}
