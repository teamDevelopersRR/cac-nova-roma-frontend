import { Menu } from './menu.model';

export const MENUITEMS: Menu[] = [
    {
        id: 'admin',
        label: 'menu.complementaryActivity',
        permission: 'ROLE_ACTIVITY',
        type: 'main',
        childrens: [
            {
                id: 'activity-list',
                state: 'activity',
                name: 'menu.activities',
                type: 'link',
                icon: 'icofont icofont-copy-alt',
                permission: 'ROLE_ACTIVITY_INSERT'
            }
        ]
    }, {
        id: 'admin',
        label: 'menu.admin',
        permission: 'ROLE_ADMIN',
        type: 'main',
        childrens: [
            {
                id: 'course-list',
                state: 'course',
                name: 'menu.courses',
                type: 'link',
                icon: 'icofont icofont-library',
                permission: 'ROLE_COURSE_INSERT'
            },
            {
                id: 'discipline-list',
                state: 'discipline',
                name: 'menu.disciplines',
                type: 'link',
                icon: 'icofont icofont-book-alt',
                permission: 'ROLE_DISCIPLINE_INSERT'
            },
            {
                id: 'activity-type-list',
                state: 'activity-type',
                name: 'menu.activityTypes',
                type: 'link',
                icon: 'icofont icofont-file-alt',
                permission: 'ROLE_ACTIVITY_TYPE_INSERT'
            },
        ]
    },
    {
        id: 'admin',
        label: 'menu.access',
        permission: 'ROLE_ADMIN',
        type: 'main',
        childrens: [
            {
                id: 'users-list',
                state: 'users',
                name: 'menu.users',
                type: 'link',
                icon: 'icofont icofont-users',
                permission: 'ROLE_USER_INSERT'
            },
            {
                id: 'profile-list',
                state: 'profile',
                name: 'menu.profiles',
                type: 'link',
                icon: 'icofont icofont-user-suited',
                permission: 'ROLE_PROFILE_INSERT'
            },
            {
                id: 'profile-type-list',
                state: 'profile-type',
                name: 'menu.profileTypes',
                type: 'link',
                icon: 'icofont icofont-user-suited',
                permission: 'ROLE_PROFILE_TYPE_INSERT'
            },
            {
                id: 'permission-list',
                state: 'permission',
                name: 'menu.permissions',
                type: 'link',
                icon: 'icofont icofont-radio-active',
                permission: 'ROLE_PERMISSION_INSERT'
            }
        ]
    },
    {
        id: 'admin',
        label: 'menu.help',
        permission: 'ROLE_ACTIVITY',
        type: 'main',
        childrens: [
            {
                id: 'manual-use',
                state: 'help',
                name: 'menu.manualUse',
                type: 'link',
                icon: 'icofont icofont-question',
                permission: 'ROLE_ACTIVITY_INSERT'
            },
            {
                id: 'location-college',
                state: 'location-college',
                name: 'menu.locationCollege',
                type: 'link',
                icon: 'icofont icofont-social-google-map',
                permission: 'ROLE_ACTIVITY_INSERT'
            }
        ]
    }
];