import { Component, Input, Output, EventEmitter } from "@angular/core";
import { NgxSmartModalService } from "ngx-smart-modal";

@Component({
  selector: "alert-confirmation",
  templateUrl: "./alert-confirmation.component.html",
  styleUrls: ["./alert-confirmation.component.scss"]
})
export class AlertConfirmationComponent {
  @Input() title: string = "alert.confirmation";
  @Input() body: string = "alert.descriptionConfirmationDelete";
  @Input() textButtonClose: string = "label.false";
  @Input() textButtonConfirmation: string = "label.true";

  @Output() eventConfirm = new EventEmitter();

  item: any;

  constructor(public ngxSmartModalService: NgxSmartModalService) {}

  openModal(item: any) {
    this.item = item;
    this.ngxSmartModalService.getModal("modal").open();
  }

  confirm() {

    let modal = undefined;

    if (this.item != true) {
      // Modal de detalhes do objeto
      modal = this.ngxSmartModalService.getModal("modalDetail");
      if (modal != undefined) modal.close();
    }

    // Modal de confirmação
    modal = this.ngxSmartModalService.getModal("modal");
    if (modal != undefined) modal.close();

    this.eventConfirm.emit(this.item);
  }
}
