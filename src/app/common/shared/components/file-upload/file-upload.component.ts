import { Component, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { AlertCenterService } from '../alert-center/service/alert-center.service';
import { TITLE_WARNING } from '../../../../util/ConstantProject';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent {

  errors: Array<string> = [];
  dragAreaClass: string = 'drag-area';

  @Input() fileExt: string = "JPG, GIF, PNG";
  @Input() maxFiles: number = 5;
  @Input() maxSize: number = 5; // 5MB
  @Input() countFiles;
  @Output() fileChange: EventEmitter<FileList[]>;

  files = [];

  constructor(public alertCenterService: AlertCenterService,
    public translate: TranslateService) {
    this.fileChange = new EventEmitter();
  }

  onFileChange(event) {
    let files = event.target.files;
    this.saveFiles(files);
  }

  @HostListener('dragover', ['$event']) onDragOver(event) {
    this.dragAreaClass = "is-dragover";
    event.preventDefault();
  }

  @HostListener('dragenter', ['$event']) onDragEnter(event) {
    this.dragAreaClass = "is-dragover";
    event.preventDefault();
  }

  @HostListener('dragend', ['$event']) onDragEnd(event) {
    this.dragAreaClass = "drag-area";
    event.preventDefault();
  }

  @HostListener('dragleave', ['$event']) onDragLeave(event) {
    this.dragAreaClass = "drag-area";
    event.preventDefault();
  }

  @HostListener('drop', ['$event']) onDrop(event) {
    this.dragAreaClass = "drag-area";

    event.preventDefault();
    event.stopPropagation();

    var files = event.dataTransfer.files;

    this.saveFiles(files);
  }

  saveFiles(files) {

    this.errors = []; // Clear error

    let isValid = this.isValidFiles(files);

    if (this.countFiles == 4) {

      this.alertCenterService.alertWarning(TITLE_WARNING, "alert.warning.fileSizeExceed", true);

    } else if (isValid) {

      this.files = files;
      this.countFiles = this.files.length;

      if(!this.isValidFiles(this.files))
        return;

      this.fileChange.emit(this.files);
    }
  }

  private isValidFiles(files) {

    // validação de quantidade de arquivos
    if (files.length > this.maxFiles) {
      let message = "";
      this.translate.get("alert.warning.youCanOnlyAddFiles").subscribe((res: string) => {
        message = res.replace("#X#", this.maxFiles + ""); // substituir por código para mensagem ser alterada
      });

      this.alertCenterService.alertWarning(TITLE_WARNING, message, false);
      return false;
    }

    return this.isValidFileExtension(files);
  }

  private isValidFileExtension(files) {

    // validação de extensões do arquivo
    var extensions = (this.fileExt.split(',')).map(function (x) { return x.toLocaleUpperCase().trim() });

    for (var i = 0; i < files.length; i++) {
      // Get file extension
      var ext = files[i].name.toUpperCase().split('.').pop() || files[i].name;
      
      // Check the extension exists
      var exists = extensions.includes(ext);
      if (!exists) {
        let message = "";
        this.translate.get("alert.warning.extensionInvalidFor").subscribe((res: string) => {
          message = res + files[i].name; // substituir por código para mensagem ser alterada
        });

        this.alertCenterService.alertWarning(TITLE_WARNING, message, false);
        return false;
      }
      
      if(!this.isValidFileSize(files[i]))
        return false;
    }
    return true;
  }

  private isValidFileSize(file) {

    // validação de tamanho do arquivo
    var fileSizeinMB = file.size / (1024 * 1000);
    var size = Math.round(fileSizeinMB * 100) / 100; // convert upto 2 decimal place

    if (size > this.maxSize) {
      let message = "";
      this.translate.get("alert.warning.fileSizeNameExceedMB").subscribe((res: string) => {
        // substituir por código para mensagem ser alterada
        message = res.replace("#FILE_NAME#", file.name).replace("#MAX_SIZE#", this.maxSize + "");
      });

      this.alertCenterService.alertDanger(TITLE_WARNING, message, false);
      return false;
    }

    return true;
  }

}
