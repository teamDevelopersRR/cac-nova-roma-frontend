import {
  Component,
  Input,
  OnDestroy,
  Inject,
  ViewEncapsulation
} from "@angular/core";
import { Spinkit } from "./spinkits";
import {
  Router,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError,
  RouterEvent
} from "@angular/router";
import { DOCUMENT } from "@angular/common";

@Component({
  selector: "spinner",
  templateUrl: "./spinner.component.html",
  styleUrls: [
    "./spinner.component.css",
    "./spinkit-css/sk-double-bounce.css",
    "./spinkit-css/sk-chasing-dots.css",
    "./spinkit-css/sk-cube-grid.css",
    "./spinkit-css/sk-line-material.css",
    "./spinkit-css/sk-rotating-plane.css",
    "./spinkit-css/sk-spinner-pulse.css",
    "./spinkit-css/sk-three-bounce.css",
    "./spinkit-css/sk-wandering-cubes.css",
    "./spinkit-css/sk-wave.css"
  ],
  encapsulation: ViewEncapsulation.None
})
export class SpinnerComponent implements OnDestroy {
  public isSpinnerVisible = true;
  public Spinkit = Spinkit;

  @Input() public backgroundColor = "#ffcc29";
  @Input() public spinner = Spinkit.skWave;

  private sub: any;

  constructor(
    private router: Router,
    @Inject(DOCUMENT) private document: Document
  ) {
    router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event);
    });
  }

  // Shows and hides the loading spinner during RouterEvent changes
  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.isSpinnerVisible = true;
    }
    if (event instanceof NavigationEnd) {
      this.isSpinnerVisible = false;
    }

    // Set loading state to false in both of the below events to hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {
      this.isSpinnerVisible = false;
    }
    if (event instanceof NavigationError) {
      this.isSpinnerVisible = false;
    }
  }

  ngOnDestroy(): void {
    this.isSpinnerVisible = false;
  }
}
