import { CardToggleDirective } from './../../shared/components/card/card-toggle.directive';
import { CardRefreshDirective } from './../../shared/components/card/card-refresh.directive';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { CardComponent } from './card/card.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { TitleComponent } from './title/title.component';
import { MenuService } from './menu/menu.service';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { AlertConfirmationComponent } from './alert-confirmation/alert-confirmation.component';
import { NgxSmartModalModule, NgxSmartModalService } from 'ngx-smart-modal';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        NgxSmartModalModule.forRoot(),
        TranslateModule
    ],
    exports: [
        BreadcrumbsComponent,
        CardComponent,
        CardRefreshDirective,
        CardToggleDirective,
        SpinnerComponent,
        TitleComponent,
        FileUploadComponent,
        AlertConfirmationComponent
    ],
    declarations: [
        BreadcrumbsComponent,
        CardComponent,
        CardRefreshDirective,
        CardToggleDirective,
        SpinnerComponent,
        TitleComponent,
        FileUploadComponent,
        AlertConfirmationComponent
    ],
    providers: [
        MenuService,
        NgxSmartModalService
    ]
})
export class ComponentsModule { }