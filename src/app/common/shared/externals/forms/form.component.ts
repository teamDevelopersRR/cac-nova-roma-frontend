import { FormUtil } from './form.util';
import { ControlValueAccessor } from '@angular/forms';
import { NgForm, RequiredValidator } from '@angular/forms';
import { Input, ChangeDetectorRef } from '@angular/core';

export class FormComponent implements ControlValueAccessor {

  @Input() form: NgForm;
  @Input() field: string;
  @Input() control: any;
  @Input() label: string;
  @Input() name: string;
  @Input() id: string;
  @Input() classErrorsDirective: boolean;
  @Input() disabled: boolean;
  @Input() extraClass: string;
  @Input() disabledError: boolean;

  errorValidation = false;
  public model = '';
  formUtil: FormUtil = new FormUtil();

  public onChangeCallback: (_: any) => void = () => { };
  private onTouchedCallback: () => void = () => { };

  constructor(public cdr: ChangeDetectorRef) { }

  // get accessor
  get value(): any {
    return this.model;
  }

  // set accessor including call the onchange callback
  set value(value: any) {
    if (value !== this.model) {
      this.model = value;
      this.onChangeCallback(value);
    }
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  // From ControlValueAccessor interface
  writeValue(value: any) {
    if (value !== this.model) {
      this.model = value;
    }
  }

  // From ControlValueAccessor interface
  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  // From ControlValueAccessor interface
  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }

  requiredError(list: any[]) {
    if (list != null) {
      list.forEach(element => {
        return (element instanceof RequiredValidator);
      });
    }
    return false;
  }

  showErrorValidation(show: boolean) {
    this.errorValidation = show;
  }
}

export interface IOption {
  id?: any;
  value: any;
  label?: string;
  hasLabel?: boolean;
}
