import { FormComponent } from './../form.component';
import { NG_VALUE_ACCESSOR, NgControl } from '@angular/forms';
import { Component, forwardRef, Input, EventEmitter, Output, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AutocompleteComponent),
      multi: true
    }
  ]
})
export class AutocompleteComponent extends FormComponent {

  @Input() isFiltered:boolean;
  @Input() type: string;
  @Input() tooltip: string;
  @Input() placementTooltip = 'top';
  @Input() placeholder: string;
  @Input() displayProperty: string;
  @Input() displayPropertyExtra: string;

  @Output() selectItem = new EventEmitter<any>();
  @Input() list: any[];
  @Output() search = new EventEmitter<any>();
  @Output() validate = new EventEmitter<any>();

  constructor(cdr: ChangeDetectorRef) {
    super(cdr);
  }

  validateEvent() {
    this.validate.emit(this.control);
  }

  clickOut() {
    this.list = [];
  }

  searchEvent() {
    this.search.emit();
  }

  select(item: any, control: NgControl) {
    this.selectItem.emit({item: item, control: control});
    this.clickOut();
  }

  bold(item: any, bold: string) {
    return item[this.displayProperty].toUpperCase().replace(bold.toUpperCase(), bold.toUpperCase().bold()) + ((this.displayPropertyExtra ? ' - ' + item[this.displayPropertyExtra] : ''));
  }
}