import { FormComponent } from './../form.component';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Component, forwardRef, ChangeDetectorRef, Input, Injectable } from '@angular/core';

import { NgbDateAdapter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

/**
 * Example of a Native Date adapter
 */
@Injectable()
export class NgbDateNativeAdapter extends NgbDateAdapter<Date> {

  fromModel(date: Date): NgbDateStruct {
    return (date && date.getFullYear) ? { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() } : null;
  }

  toModel(date: NgbDateStruct): Date {
    return date ? new Date(date.year, date.month - 1, date.day) : null;
  }
}

@Component({
  selector: 'app-input-date',
  templateUrl: './input-date.component.html',
  styleUrls: ['./input-date.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputDateComponent),
      multi: true
    },
    { provide: NgbDateAdapter, useClass: NgbDateNativeAdapter }
  ]
})
export class InputDateComponent extends FormComponent {

  @Input() tooltip: string;
  @Input() placeholder: string;
  min: Date = new Date(2018,1,1);
  max: Date = new Date();

  constructor(cdr: ChangeDetectorRef) {
    super(cdr);
  }

  getMinDate(){
    return this.min.getFullYear()+"-01-01";
  }

  getMaxDate(){

    let day = "";
    if(this.max.getDate() < 10){
      day = "0"+this.max.getDate();
    } else {
      day = ""+this.max.getDate();
    }

    let month = "";
    if(this.max.getMonth()+1 < 10){
      month = "0"+(this.max.getMonth()+1);
    } else {
      month = ""+(this.max.getMonth()+1);
    }

    return this.max.getFullYear()+"-"+month+"-"+day;
  }

  parseDate(date: any): Date {
    if (date) {
      return new Date(date.year, date.month, date.day);
    } else {
      return null;
    }
  }

}
