import { FormComponent } from './../form.component';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Component, forwardRef, Input, EventEmitter, Output, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-input-check',
  templateUrl: './input-check.component.html',
  styleUrls: ['./input-check.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputCheckComponent),
      multi: true
    }
  ]
})
export class InputCheckComponent extends FormComponent {

  @Input() accept: string;
  @Input() tooltip: string;
  @Input() placementTooltip = 'top';

  @Output() validate = new EventEmitter<any>();

  constructor(cdr: ChangeDetectorRef) {
    super(cdr);
  }

  validateEvent() {
    this.validate.emit(this.control);
  }

  check(event) {
    console.log(event);
  }
}