import { ErrorValidation } from './../../../../../http-interceptor/model/error-validation.model';
import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class ErrorValidationService {

    private notifierErrorValidation = new EventEmitter<ErrorValidation[]>();

    constructor() {
    }

    getErrorValidation() {
        return this.notifierErrorValidation;
    }

    setErrorsValidation(errorValidation) {
        this.notifierErrorValidation.emit(errorValidation);
    }

}