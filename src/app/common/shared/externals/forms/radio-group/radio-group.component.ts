import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FormComponent, IOption } from './../form.component';
import { Component, Input, ChangeDetectorRef, forwardRef } from '@angular/core';

@Component({
  selector: 'app-radio-group',
  templateUrl: './radio-group.component.html',
  styleUrls: ['./radio-group.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RadioGroupComponent),
      multi: true
    }
  ]
})
export class RadioGroupComponent extends FormComponent {

  @Input() modelList: boolean;
  @Input() list: IOption[];

  constructor(cdr: ChangeDetectorRef) {
    super(cdr);
  }

  ngAfterViewInit() {

  }

}