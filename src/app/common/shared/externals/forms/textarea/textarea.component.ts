import { FormComponent } from './../form.component';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Component, forwardRef, Input, EventEmitter, Output, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextareaComponent),
      multi: true
    }
  ]
})
export class TextareaComponent extends FormComponent {

  @Input() tooltip: string;
  @Input() placementTooltip = 'top';
  @Input() placeholder: string;
  @Input() readonly: boolean;
  @Input() maxlength: boolean;
  @Input() rows: number;

  @Output() validate = new EventEmitter<any>();

  constructor(cdr: ChangeDetectorRef) {
    super(cdr);
  }

  validateEvent() {
    this.validate.emit(this.control);
  }
}