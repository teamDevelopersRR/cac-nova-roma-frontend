import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'semester'
})
export class SemesterPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value) {
      let test = parseInt(value);
      if (test) {
        if (value.length == 4) return value + ".";
        else if (value.length > 5) return value.split(".")[0] + "." + value.split(".")[1];
        return value;
      } else {
        return value;
      }
    }
  }

}