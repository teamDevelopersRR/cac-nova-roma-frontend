import { MaskModule } from './mask/mask.module';
import { NgIfPermissionDirective } from './directives/ng-if-permission.directive';
import { AutocompleteComponent } from './forms/autocomplete/autocomplete.component';
import { SearchDirective } from './directives/search.directive';
import { MaskService } from './mask/mask.service';
import { RadioGroupComponent } from './forms/radio-group/radio-group.component';
import { SelectComponent } from './forms/select/select.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { InputDateComponent } from './forms/input-date/input-date.component';
import { KeysPipe } from './pipes/keys.pipe';
import { FieldErrorComponent } from './forms/field-error/field-error.component';
import { FormsModule } from '@angular/forms';
import { InputComponent } from './forms/input/input.component';
import { InputGroupComponent } from './forms/input-group/input-group.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextareaComponent } from './forms/textarea/textarea.component';
import { InputCheckComponent } from './forms/input-check/input-check.component';
import { DateFormatPipe } from './pipes/date-format.pipe';
import { DateTimeFormatPipe } from './pipes/date-time-format.pipe';
import { Ng2BRPipesModule } from 'ng2-brpipes';
import { TranslateModule } from '@ngx-translate/core';
import { SemesterPipe } from './pipes/semester.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule.forRoot(),
    MaskModule,
    Ng2BRPipesModule,
    TranslateModule
  ],
  declarations: [
    FieldErrorComponent,
    InputGroupComponent,
    InputDateComponent,
    InputComponent,
    SelectComponent,
    AutocompleteComponent,
    RadioGroupComponent,
    KeysPipe,
    DateFormatPipe,
    DateTimeFormatPipe,
    TextareaComponent,
    SearchDirective,
    InputCheckComponent,
    NgIfPermissionDirective,
    SemesterPipe
  ],
  exports: [
    FieldErrorComponent,
    InputGroupComponent,
    InputDateComponent,
    InputComponent,
    SelectComponent,
    AutocompleteComponent,
    RadioGroupComponent,
    KeysPipe,
    DateFormatPipe,
    DateTimeFormatPipe,
    TextareaComponent,
    SearchDirective,
    InputCheckComponent,
    NgIfPermissionDirective,
    MaskModule,
    Ng2BRPipesModule,
    SemesterPipe
  ],
  providers: [MaskService]
})
export class ExternalModule { }