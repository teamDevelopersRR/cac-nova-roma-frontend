import { NgModule } from '@angular/core';
import { MaskDirective } from './mask.directive';
import { MaskService } from './mask.service';

@NgModule({
  providers: [
    MaskService
  ],
  declarations: [
    MaskDirective
  ],
  exports: [
    MaskDirective
  ]
})
export class MaskModule {}
