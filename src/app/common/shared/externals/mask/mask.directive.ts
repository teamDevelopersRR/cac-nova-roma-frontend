import { Directive, Input, ElementRef, NgZone, AfterContentInit, OnDestroy, OnChanges } from '@angular/core';
import { MaskService } from './mask.service';
import { NgControl } from '@angular/forms';

declare let $: any;

@Directive({
  selector: '[mask]',
  host: {
    '(blur)': 'onBlur()'
  }
})
export class MaskDirective {

  @Input() options: Object = {};
  @Input() mask: string;

  constructor(private el: ElementRef, private zone: NgZone, private control: NgControl,
              private maskService: MaskService) {

    $.jMaskGlobals = {
      translation: {
        '0': { pattern: /\d/ },
        '9': { pattern: /\d/, optional: true },
        '#': { pattern: /\d/, recursive: true },
        'A': { pattern: /[a-zA-Z0-9]/ },
        'S': { pattern: /[a-zA-Z]/ },
        'Y': { pattern: /[0-9]/ }
      }
    };
  }

  private toCamelCase(text: string): string {
    return text.replace(/^([A-Z])|\s(\w)/g, function (match, p1, p2, offset) {
      if (p2) {
        return p2.toUpperCase();
      }
      return p1.toLowerCase();
    });
  }

  private getPattern(pattern: any): string {
    const rule = this.toCamelCase('get ' + this.mask);
    if (rule in this.maskService) {
      pattern = (<any>this.maskService)[rule]();
    }
    return pattern;
  }

  ngAfterContentInit() {
    this.zone.run(() => {
      if (this.mask !== undefined) {
        $(this.el.nativeElement).mask(this.getPattern(this.mask), this.options);
        if (this.control.control) {
          this.control.control.setValue(this.el.nativeElement.value);
        }
      }
    });
  }

  ngOnDestroy() {
    $(this.el.nativeElement).unmask();
  }

  ngOnChanges(changes: any) {
    if (changes.pattern) {
      if (typeof (changes.pattern.previousValue) === 'string') {
        $(this.el.nativeElement).mask(this.getPattern(changes.pattern.currentValue), this.options);
      }
    }
  }

  onBlur() {
    this.zone.run(() => {
      if (this.control.control) {
        this.control.control.setValue(this.el.nativeElement.value);
      }
    });
  }
}
