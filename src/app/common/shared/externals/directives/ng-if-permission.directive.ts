import { AuthenticationService } from './../../../authentication/authentication.service';
import { Directive, TemplateRef, ViewContainerRef, Input } from '@angular/core';

@Directive({
  selector: '[ngIfPermission]'
})
export class NgIfPermissionDirective {

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private authenticationService: AuthenticationService) { }

  @Input() set ngIfPermission([...args]) {
    const permissions: string[] = (args instanceof Array) ? args : [args];
    let show = false;
    permissions.forEach(role => {
      if (this.authenticationService.hasPermission(role)) {
        show = true;
        return;
      }
    });
    if (show) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainer.clear();
    }
  }

}