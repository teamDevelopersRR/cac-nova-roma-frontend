/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { NgIfPermissionDirective } from './ng-if-permission.directive';

describe('Directive: NgIfPermission', () => {
  it('should create an instance', () => {
    const directive = new NgIfPermissionDirective();
    expect(directive).toBeTruthy();
  });
});