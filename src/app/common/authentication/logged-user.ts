import { Profile } from "../../layouts/admin/profile/model/profile.model";

export class LoggedUser {
    id: number;
    login: string;
    name: string;
    roles: string[];
    token?:string;
    profile:Profile;
    typeProfile: string;
}
