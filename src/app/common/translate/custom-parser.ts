import { TranslateDefaultParser } from '@ngx-translate/core';

export class InterpolatedCustomTranslateParser extends TranslateDefaultParser {
    constructor(private group: string) {
        super();
    }

    getValue(target: any, key: string): any {
        return TranslateDefaultParser.prototype.getValue(target, (this.group + "." + key));
    }
}