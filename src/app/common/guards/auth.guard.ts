import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { AuthenticationService } from './../authentication/authentication.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(public router: Router, public authService: AuthenticationService) { }

    public canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (this.checkAuthenticated()) {
            const permissions: string[] = (route.data.permissions) ? ((route.data.permissions instanceof Array) ? route.data.permissions : [route.data.permissions]) : [];
            if(permissions && permissions.length > 0) {
                let show = false;
                permissions.forEach(role => {
                    if (this.authService.hasPermission(role)) {
                        show = true;
                    }
                });
                if (!show) {
                    this.router.navigate(['authentication/403']);
                }
                return show;
            } else {
                return true;
            }
        }
    }

    checkAuthenticated() {
        if (this.authService.isLoggedIn()) {
            return true;
        } else {
            this.router.navigate(['authentication/login']);
            return false;
        }
    }

}