import { ErrorValidationService } from './../../../../common/shared/externals/forms/field-error/service/error-validation.service';
import { Permission } from './../model/permission.model';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { Form } from '../../form';
import { PermissionService } from '../service/permission.service';
import { AlertCenterService } from '../../../../common/shared/components/alert-center/service/alert-center.service';
import { TITLE_ERROR, MESSAGE_OPERATION_ERROR } from '../../../../util/ConstantProject';

@Component({
  selector: 'app-permission-form',
  templateUrl: './permission-form.component.html',
  styleUrls: ['./permission-form.component.scss']
})
export class PermissionFormComponent extends Form implements OnInit {

  constructor(public translate: TranslateService,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public errorValidationService: ErrorValidationService,
    public alertCenterService: AlertCenterService,
    public permissionService: PermissionService) {
    super(translate, router, activeRoute, 'permission', errorValidationService, alertCenterService);
  }

  permission: Permission = new Permission();

  ngOnInit() {
    if (!this.checkEdit()) {

    }
  }

  preparateObject() {
    this.isLoading = true;
    const copy = Object.assign({}, this.permission);
    return copy;
  }

  insert(): void {
    this.permissionService.insert(this.preparateObject()).then(
      permission => {
        if ((<any>permission).exception)
          this.errorValidation(permission);
        else this.callbackInsertOrUpdate();
      }, (response: Response) => {
        this.errorValidation(response);
      }
    );
  }

  update(): void {
    this.permissionService.update(this.preparateObject()).then(
      permission => {
        if ((<any>permission).exception)
          this.errorValidation(permission);
        else this.callbackInsertOrUpdate();
      }, (response: Response) => {
        this.errorValidation(response);
      }
    );
  }

  findById(id: any) {
    return this.permissionService.findById(id).then(
      permission => {
        this.permission = permission;
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
        console.log(error);
      }
    );
  }

}
