import { Component, OnInit } from '@angular/core';
import { PermissionService } from '../service/permission.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { List } from '../../list';
import { AlertCenterService } from '../../../../common/shared/components/alert-center/service/alert-center.service';
import { TITLE_ERROR, MESSAGE_OPERATION_ERROR, TITLE_SUCCESS, MESSAGE_OPERATION_SUCCESS } from '../../../../util/ConstantProject';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Permission } from '../model/permission.model';

@Component({
  selector: 'app-permission-list',
  templateUrl: './permission-list.component.html',
  styleUrls: ['./permission-list.component.scss']
})
export class PermissionListComponent extends List implements OnInit {

  dataProvider = [];
  selectedPermission: Permission = new Permission();

  constructor(public translate: TranslateService,
    public router: Router,
    public activeRoute: ActivatedRoute,
    private permissionService: PermissionService,
    public alertCenterService:AlertCenterService,
    public ngxSmartModalService: NgxSmartModalService) {
    super(translate, router, activeRoute, "permission", alertCenterService, ngxSmartModalService);
  }

  ngOnInit() {
    this.loadDataProvider();
  }

  async loadDataProvider() {
    this.dataProvider = [];
    await this.permissionService.getAll().then((item) => this.dataProvider.push(item));
    this.isLoaded = true;
  }

  edit(item){
    this.router.navigate([this.routeEdit+item.id]);
  }

  confirmDelete(item){
    this.permissionService.delete(item).then(
      item => {
        this.loadDataProvider();
        this.alertCenterService.alertSuccess(TITLE_SUCCESS, MESSAGE_OPERATION_SUCCESS);
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
        console.log(error);
      }
    );
  }

  openModalDetails(item){
    this.selectedPermission = item;
    this.openModal();
  }

}
