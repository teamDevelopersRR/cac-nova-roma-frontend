import { Permission } from './../model/permission.model';
import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AuthenticationService } from '../../../../common/authentication/authentication.service';
import { LoggedUser } from '../../../../common/authentication/logged-user';

@Injectable()
export class PermissionService {

  constructor(private http: HttpClient,
    private authenticationService: AuthenticationService) { }

  async insert(permission: Permission) {
    return await this.http.post<Permission>(`${environment.url_application}/permission/`, permission).toPromise();
  }

  async update(permission: Permission) {
    return await this.http.put<Permission>(`${environment.url_application}/permission/`, permission).toPromise();
  }

  async delete(permission: Permission) {
    return await this.http.delete<Permission>(`${environment.url_application}/permission/${permission.id}`).toPromise();
  }

  async getAll() {
    return await this.http.get<Permission[]>(`${environment.url_application}/permission/`).toPromise();
  }

  async findPermissionByProfileUserLogged() {
    let loggedUser: LoggedUser = this.authenticationService.getLoggedUser();
    return await this.http.get<Permission[]>(`${environment.url_application}/permission/profile/${loggedUser.profile.id}`).toPromise();
  }

  async findById(id: number) {
    return await this.http.get<Permission>(`${environment.url_application}/permission/${id}`).toPromise();
  }

  async findByName(name) {
    const params = new HttpParams().set('name', name);

    return await this.http.post<Permission[]>(`${environment.url_application}/permission/getByName?`, null,
      {
        params: params,
        observe: 'response'
      }
    ).toPromise();
  }

}
