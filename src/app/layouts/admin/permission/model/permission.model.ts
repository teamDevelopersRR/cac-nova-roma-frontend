import { Basic } from "./../../models/basic.model";

export class Permission extends Basic {
    name: string;
    crud: boolean;
}