import { PermissionListComponent } from './permission-list/permission-list.component';
import { PermissionFormComponent } from './permission-form/permission-form.component';
import { Routes } from '@angular/router';
import { AuthGuard } from '../../../common/guards/auth.guard';

export const permissionRoutes: Routes = [
    {
        path: '',
        component: PermissionListComponent,
        data: {
            breadcrumb: 'breadcrumb.permissions',
            permissions: ['ROLE_PERMISSION_READ']
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'edit/:id',
        component: PermissionFormComponent,
        data: {
            breadcrumb: 'breadcrumb.permissionEdit',
            permissions: ['ROLE_PERMISSION_UPDATE']
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'new',
        component: PermissionFormComponent,
        data: {
            breadcrumb: 'breadcrumb.permissionNew',
            permissions: ['ROLE_PERMISSION_INSERT']
        },
        canActivate: [AuthGuard]
    }
];