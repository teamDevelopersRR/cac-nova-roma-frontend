import { RouterModule } from '@angular/router';
import { AppCommonModule } from './../../../common/app-common.module';
import { NgModule } from '@angular/core';
import { PermissionFormComponent } from './permission-form/permission-form.component';
import { PermissionListComponent } from './permission-list/permission-list.component';
import { permissionRoutes } from './permission.routing';
import { PermissionService } from './service/permission.service';
import { NgxSmartModalModule } from 'ngx-smart-modal';

@NgModule({
  imports: [
    AppCommonModule,
    RouterModule.forChild(permissionRoutes),
    NgxSmartModalModule.forRoot()
  ],
  declarations: [
    PermissionFormComponent,
    PermissionListComponent
  ],
  providers: [PermissionService]
})
export class PermissionModule { }