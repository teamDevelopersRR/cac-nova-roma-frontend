import { Util } from './../../util/util';
import { ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertCenterService } from '../../common/shared/components/alert-center/service/alert-center.service';
import { AlertConfirmationComponent } from '../../common/shared/components/alert-confirmation/alert-confirmation.component';
import { NgxSmartModalService } from 'ngx-smart-modal';

export abstract class List {

    @ViewChild(AlertConfirmationComponent) deleteModal;

    constructor(public translate: TranslateService,
        public router: Router,
        public activeRoute: ActivatedRoute,
        public route: string,
        public alertCenterService: AlertCenterService,
        public ngxSmartModalService: NgxSmartModalService) { }

    public routeEdit: String = "/" + this.route + "/edit/";

    util: Util = new Util();

    isLoaded = false;

    abstract ngOnInit(): void;
    abstract loadDataProvider(): void;
    abstract edit(item): void;
    abstract confirmDelete(item): void;

    openModal() {
        this.ngxSmartModalService.getModal('modalDetail').open();
    }

    closeModal() {
        this.ngxSmartModalService.getModal('modalDetail').close();
    }

    delete(item) {
        this.deleteModal.openModal(item);
    }

}