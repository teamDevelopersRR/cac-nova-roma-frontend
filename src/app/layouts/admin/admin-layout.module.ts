import { AppCommonModule } from './../../common/app-common.module';
import { AdminLayoutComponent } from './admin-layout.component';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    AppCommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    ReactiveFormsModule
  ],
  declarations: [
    AdminLayoutComponent
  ]
})

export class AdminLayoutModule { }

