import { Basic } from "../../models/basic.model";

export class Course extends Basic {
    code: string;
    name: string;
    numberHourMax: number;
}