import { Course } from "../../model/course.model";
import { Discipline } from "../../../discipline/model/discipline.model";
import { Basic } from "../../../models/basic.model";

export class CourseDiscipline extends Basic {
    course: Course = new Course();
    discipline: Discipline = new Discipline();
}
