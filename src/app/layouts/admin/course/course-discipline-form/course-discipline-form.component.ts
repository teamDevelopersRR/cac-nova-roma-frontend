import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ErrorValidationService } from '../../../../common/shared/externals/forms/field-error/service/error-validation.service';
import { Form } from '../../form';
import { Course } from '../model/course.model';
import { CourseDisciplineService } from './service/course-discipline.service';
import { CourseDiscipline } from './model/course-discipline.model';
import { Discipline } from '../../discipline/model/discipline.model';
import { DisciplineService } from '../../discipline/service/discipline.service';
import { CourseService } from '../service/course.service';
import { AlertCenterService } from '../../../../common/shared/components/alert-center/service/alert-center.service';
import { TITLE_ERROR, MESSAGE_OPERATION_ERROR } from '../../../../util/ConstantProject';

@Component({
  selector: 'app-course-discipline-form',
  templateUrl: './course-discipline-form.component.html',
  styleUrls: ['./course-discipline-form.component.scss']
})
export class CourseDisciplineFormComponent extends Form {

  constructor(public translate: TranslateService,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public errorValidationService: ErrorValidationService,
    public alertCenterService:AlertCenterService,
    public courseDisciplineService:CourseDisciplineService,
    public disciplineService: DisciplineService,
    public courseService: CourseService) {
    super(translate, router, activeRoute, 'course-discipline', errorValidationService, alertCenterService);
  }

  public course:Course = new Course();
  public discipline:Discipline = new Discipline();
  public selectedDiscipline:Discipline = new Discipline ();
  public courseDiscipline:CourseDiscipline = new CourseDiscipline();

  disciplines = [];
  isFiltered = false;

  ngOnInit() {
    this.isFiltered = true;

    if (!this.checkEdit()) {
      this.findByCourseId(this.activeRoute.params["_value"]["id-course"]);
    }
  }
  
  findByCourseId(id: any) {
    return this.courseService.findById(id).then(
      course => {
        this.course = course;
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
        console.log(error);
      }
    );
  }

  preparateObject() {
    this.isLoading = true;
    const copy = Object.assign({}, this.courseDiscipline);
    return copy;
  }

  insert(): void {
    this.courseDiscipline.course = this.course;
    this.courseDiscipline.discipline = this.discipline;
    
    this.courseDisciplineService.insert(this.preparateObject()).then(
      courseDiscipline => {
        if ((<any>courseDiscipline).exception)
          this.errorValidation(courseDiscipline);
        else this.callbackInsertOrUpdate();
      }, (response: Response) => {
        this.errorValidation(response);
      }
    );
  }

  update(): void {
    this.courseDisciplineService.update(this.preparateObject()).then(
      courseDiscipline => {
        if ((<any>courseDiscipline).exception)
          this.errorValidation(courseDiscipline);
        else this.callbackInsertOrUpdate();
      }, (response: Response) => {
        this.errorValidation(response);
      }
    );
  }

  findById(id: any) {
    return this.courseDisciplineService.findById(id).then(
      courseDiscipline => {
        this.courseDiscipline = courseDiscipline;

        this.course = courseDiscipline.course;
        this.discipline = courseDiscipline.discipline;
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
        console.log(error);
      }
    );
  }

  comeback(){
    this.router.navigate(["course/edit/"+this.course.id]);
  }
  
  async findDiscipline() {
    this.selectedDiscipline = new Discipline();
    this.isFiltered = false;
    await this.disciplineService.findByName(this.discipline.name)
      .then(data => {
        this.isFiltered = true;
        this.disciplines = data.body;
      });
  }

  selectDiscipline(event) {
    if (event.item) {
      this.discipline = event.item;
      this.selectedDiscipline = event.item;
    }
  }

}
