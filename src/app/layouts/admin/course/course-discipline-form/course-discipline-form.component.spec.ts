import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {} from 'jasmine';

import { CourseDisciplineFormComponent } from './course-discipline-form.component';

describe('CourseDisciplineFormComponent', () => {
  let component: CourseDisciplineFormComponent;
  let fixture: ComponentFixture<CourseDisciplineFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseDisciplineFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseDisciplineFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
