import { TestBed, inject } from '@angular/core/testing';

import { CourseDisciplineService } from './course-discipline.service';

describe('CourseDisciplineService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CourseDisciplineService]
    });
  });

  it('should be created', inject([CourseDisciplineService], (service: CourseDisciplineService) => {
    expect(service).toBeTruthy();
  }));
});
