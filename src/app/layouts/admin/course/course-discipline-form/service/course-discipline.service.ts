import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CourseDiscipline } from '../model/course-discipline.model';
import { environment } from '../../../../../../environments/environment';

@Injectable()
export class CourseDisciplineService {

    constructor(private http: HttpClient) { }

    async insert(courseDiscipline: CourseDiscipline) {
      return await this.http.post<CourseDiscipline>(`${environment.url_application}/courseDiscipline/`, courseDiscipline).toPromise();
    }
  
    async update(courseDiscipline: CourseDiscipline) {
      return await this.http.put<CourseDiscipline>(`${environment.url_application}/courseDiscipline/`, courseDiscipline).toPromise();
    }
  
    async delete(courseDiscipline: CourseDiscipline) {
      return await this.http.delete<CourseDiscipline>(`${environment.url_application}/courseDiscipline/${courseDiscipline.id}`).toPromise();
    }
  
    async getAll() {
      return await this.http.get<CourseDiscipline[]>(`${environment.url_application}/courseDiscipline/`).toPromise();
    }
  
    async findById(id: number) {
      return await this.http.get<CourseDiscipline>(`${environment.url_application}/courseDiscipline/${id}`).toPromise();
    }

    async findByCourse(id: number) {
      return await this.http.get<CourseDiscipline[]>(`${environment.url_application}/courseDiscipline/getByCourse/${id}`).toPromise();
    }
  
}