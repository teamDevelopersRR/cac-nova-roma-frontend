import { Component, OnInit } from '@angular/core';
import { CourseService } from '../service/course.service';
import { Router, ActivatedRoute } from '@angular/router';
import { List } from '../../list';
import { TranslateService } from '@ngx-translate/core';
import { AlertCenterService } from '../../../../common/shared/components/alert-center/service/alert-center.service';
import { TITLE_ERROR, MESSAGE_OPERATION_ERROR, TITLE_SUCCESS, MESSAGE_OPERATION_SUCCESS } from '../../../../util/ConstantProject';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Course } from '../model/course.model';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.scss']
})
export class CourseListComponent extends List implements OnInit {

  dataProvider = [];
  selectedCourse: Course = new Course();

  constructor(public translate: TranslateService,
    public router: Router,
    public activeRoute: ActivatedRoute,
    private courseService: CourseService,
    public alertCenterService:AlertCenterService,
    public ngxSmartModalService: NgxSmartModalService) {
    super(translate, router, activeRoute, "course", alertCenterService, ngxSmartModalService);
  }

  ngOnInit() {
    this.loadDataProvider();
  }

  async loadDataProvider() {
    this.dataProvider = [];
    await this.courseService.getAll().then((item) => this.dataProvider.push(item));
    this.isLoaded = true;
  }

  edit(item){
    this.router.navigate([this.routeEdit+item.id]);
  }

  confirmDelete(item){
    this.courseService.delete(item).then(
      item => {
        this.loadDataProvider();
        this.alertCenterService.alertSuccess(TITLE_SUCCESS, MESSAGE_OPERATION_SUCCESS);
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
        console.log(error);
      }
    );
  }

  openModalDetails(item){
    this.selectedCourse = item;
    this.openModal();
  }

}
