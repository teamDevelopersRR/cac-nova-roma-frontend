import { CourseListComponent } from './course-list/course-list.component';
import { CourseFormComponent } from './course-form/course-form.component';
import { Routes } from '@angular/router';
import { CourseDisciplineFormComponent } from './course-discipline-form/course-discipline-form.component';
import { AuthGuard } from '../../../common/guards/auth.guard';

export const courseRoutes: Routes = [
    {
        path: '',
        component: CourseListComponent,
        data: {
            breadcrumb: 'breadcrumb.courses',
            permissions: ['ROLE_COURSE_READ']
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'edit/:id',
        component: CourseFormComponent,
        data: {
            breadcrumb: 'breadcrumb.courseEdit',
            permissions: ['ROLE_COURSE_UPDATE', 'ROLE_COURSE_DISCIPLINE_READ']
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'new',
        component: CourseFormComponent,
        data: {
            breadcrumb: 'breadcrumb.courseNew',
            permissions: ['ROLE_COURSE_INSERT', 'ROLE_COURSE_DISCIPLINE_READ']
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'edit/:id-course/course-discipline/edit/:id',
        component: CourseDisciplineFormComponent,
        data: {
            breadcrumb: 'breadcrumb.courseDisciplineEdit',
            permissions: ['ROLE_COURSE_DISCIPLINE_UPDATE']
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'edit/:id-course/course-discipline/new',
        component: CourseDisciplineFormComponent,
        data: {
            breadcrumb: 'breadcrumb.courseDisciplineNew',
            permissions: ['ROLE_COURSE_DISCIPLINE_INSERT']
        },
        canActivate: [AuthGuard]
    }
];