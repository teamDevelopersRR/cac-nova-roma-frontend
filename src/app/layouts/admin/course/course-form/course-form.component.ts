import { ErrorValidationService } from './../../../../common/shared/externals/forms/field-error/service/error-validation.service';
import { Course } from './../model/course.model';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { CourseService } from '../service/course.service';
import { CourseDisciplineService } from '../course-discipline-form/service/course-discipline.service';
import { AlertCenterService } from '../../../../common/shared/components/alert-center/service/alert-center.service';
import { TITLE_ERROR, MESSAGE_OPERATION_ERROR, TITLE_SUCCESS, MESSAGE_OPERATION_SUCCESS } from '../../../../util/ConstantProject';
import { FormList } from '../../form-list';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { CourseDiscipline } from '../course-discipline-form/model/course-discipline.model';

@Component({
  selector: 'app-course-form',
  templateUrl: './course-form.component.html',
  styleUrls: ['./course-form.component.scss']
})
export class CourseFormComponent extends FormList implements OnInit {

  course: Course = new Course();
  selectedCourseDiscipline: CourseDiscipline = new CourseDiscipline();

  titleAba: string = "";

  dataProvider = [];

  constructor(public translate: TranslateService,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public errorValidationService: ErrorValidationService,
    public alertCenterService: AlertCenterService,
    public courseService: CourseService,
    public courseDisciplineService: CourseDisciplineService,
    public ngxSmartModalService: NgxSmartModalService) {
    super(translate, router, activeRoute, 'course', errorValidationService, alertCenterService, ngxSmartModalService);
  }

  ngOnInit() {
    if (!this.checkEdit()) {

    }
  }

  preparateObject() {
    this.isLoading = true;
    const copy = Object.assign({}, this.course);
    return copy;
  }

  insert(): void {
    this.courseService.insert(this.preparateObject()).then(
      course => {
        if ((<any>course).exception) this.errorValidation(course);
        else this.callbackInsertOrUpdate();
      }, (response: Response) => {
        this.errorValidation(response);
      }
    );
  }

  update(): void {
    this.courseService.update(this.preparateObject()).then(
      course => {
        if ((<any>course).exception) this.errorValidation(course);
        else this.callbackInsertOrUpdate();
      }, (response: Response) => {
        this.errorValidation(response);
      }
    );
  }

  edit(item): void {
    this.router.navigate([this.routeEdit + String(this.course.id) + "/course-discipline/edit/" + item.id]);
  }

  async getCourseDisciplineByCourse() {
    this.dataProvider = [];
    await this.courseDisciplineService.findByCourse(this.course.id).then((item) => this.dataProvider.push(item));
    this.isLoaded = true;
  }

  changeInput(field, $event) {
    switch (field) {
      case 'numberHourMax':
        this.course.numberHourMax = parseInt(this.course.numberHourMax + "");
        break;
    }
  }

  findById(id: any) {
    return this.courseService.findById(id).then(
      course => {
        this.course = course;
        this.titleAba = this.course.name;

        this.getCourseDisciplineByCourse();
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
        console.log(error);
      }
    );
  }

  confirmDelete(item) {
    this.courseDisciplineService.delete(item).then(
      item => {
        this.getCourseDisciplineByCourse();
        this.alertCenterService.alertSuccess(TITLE_SUCCESS, MESSAGE_OPERATION_SUCCESS);
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
        console.log(error);
      }
    );
  }

  openModalDetails(item) {
    this.selectedCourseDiscipline = item;
    this.openModal();
  }

}
