import { RouterModule } from '@angular/router';
import { AppCommonModule } from './../../../common/app-common.module';
import { NgModule } from '@angular/core';
import { CourseFormComponent } from './course-form/course-form.component';
import { CourseListComponent } from './course-list/course-list.component';
import { courseRoutes } from './course.routing';
import { CourseService } from './service/course.service';
import { CourseDisciplineFormComponent } from './course-discipline-form/course-discipline-form.component';
import { CourseDisciplineService } from './course-discipline-form/service/course-discipline.service';
import { DisciplineService } from '../discipline/service/discipline.service';
import { NgxSmartModalModule } from 'ngx-smart-modal';

@NgModule({
  imports: [
    AppCommonModule,
    RouterModule.forChild(courseRoutes),
    NgxSmartModalModule.forRoot()
  ],
  declarations: [
    CourseFormComponent,
    CourseListComponent,
    CourseDisciplineFormComponent
  ],
  providers: [CourseService, CourseDisciplineService, DisciplineService]
})
export class CourseModule { }