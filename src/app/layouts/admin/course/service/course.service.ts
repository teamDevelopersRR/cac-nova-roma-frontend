import { Course } from './../model/course.model';
import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class CourseService {

  constructor(private http: HttpClient) { }

  async insert(course: Course) {
    return await this.http.post<Course>(`${environment.url_application}/course/`, course).toPromise();
  }

  async update(course: Course) {
    return await this.http.put<Course>(`${environment.url_application}/course/`, course).toPromise();
  }
  
  async delete(course: Course) {
    return await this.http.delete<Course>(`${environment.url_application}/course/${course.id}`).toPromise();
  }

  async getAll() {
    return await this.http.get<Course[]>(`${environment.url_application}/course/`).toPromise();
  }

  async findById(id: number) {
    return await this.http.get<Course>(`${environment.url_application}/course/${id}`).toPromise();
  }

  async findByName(name) {
    const params = new HttpParams().set('name', name);

      return await this.http.post<Course[]>(`${environment.url_application}/course/getByName?`, null,
          {
              params: params,
              observe: 'response'
          }
      ).toPromise();
  }

}
