import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { ProfileType } from '../model/profile-type.model';

@Injectable()
export class ProfileTypeService {

  constructor(private http: HttpClient) { }

  async insert(profileType: ProfileType) {
    return await this.http.post<ProfileType>(`${environment.url_application}/profileType/`, profileType).toPromise();
  }

  async update(profileType: ProfileType) {
    return await this.http.put<ProfileType>(`${environment.url_application}/profileType/`, profileType).toPromise();
  }
  
  async delete(profileType: ProfileType) {
    return await this.http.delete<ProfileType>(`${environment.url_application}/profileType/${profileType.id}`).toPromise();
  }

  async getAll() {
    return await this.http.get<ProfileType[]>(`${environment.url_application}/profileType/`).toPromise();
  }

  async findById(id: number) {
    return await this.http.get<ProfileType>(`${environment.url_application}/profileType/${id}`).toPromise();
  }

}
