import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {} from 'jasmine';

import { ProfileTypeFormComponent } from './profile-type-form.component';

describe('ProfileTypeFormComponent', () => {
  let component: ProfileTypeFormComponent;
  let fixture: ComponentFixture<ProfileTypeFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileTypeFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileTypeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
