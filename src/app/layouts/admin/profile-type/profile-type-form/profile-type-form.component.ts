import { Component, OnInit } from '@angular/core';
import { ProfileType } from '../model/profile-type.model';
import { Form } from '../../form';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ErrorValidationService } from '../../../../common/shared/externals/forms/field-error/service/error-validation.service';
import { ProfileTypeService } from '../service/profile-type.service';
import { IOption } from '../../../../common/shared/externals/forms/form.component';
import { AlertCenterService } from '../../../../common/shared/components/alert-center/service/alert-center.service';
import { TITLE_ERROR, MESSAGE_OPERATION_ERROR } from '../../../../util/ConstantProject';

@Component({
  selector: 'app-profile-type-form',
  templateUrl: './profile-type-form.component.html',
  styleUrls: ['./profile-type-form.component.scss']
})
export class ProfileTypeFormComponent extends Form implements OnInit {
  
  constructor(public translate: TranslateService,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public errorValidationService: ErrorValidationService,
    public alertCenterService:AlertCenterService,
    public profileTypeService: ProfileTypeService) {
    super(translate, router, activeRoute, 'profile-type', errorValidationService, alertCenterService);
  }

  listCategory: IOption[] = [
    {
      id: 1,
      label: 'ADMIN',
      hasLabel: true,
      value: 'ADMIN'
    },
    {
      id: 2,
      label: 'STUDENT',
      hasLabel: true,
      value: 'STUDENT'
    },
    {
      id: 3,
      label: 'SECRETARY',
      hasLabel: true,
      value: 'SECRETARY'
    }
  ];

  public profileType:ProfileType = new ProfileType();

  ngOnInit() {
    if(!this.checkEdit()){
      
    }
  }

  preparateObject() {
    this.isLoading = true;
    const copy = Object.assign({}, this.profileType);
    return copy;
  }

  insert(): void {
    this.profileTypeService.insert(this.preparateObject()).then(
      profileType => {
        if ((<any>profileType).exception)
          this.errorValidation(profileType);
        else this.callbackInsertOrUpdate();
      }, (response: Response) => {
        this.errorValidation(response);
      }
    );
  }

  update(): void {
    this.profileTypeService.update(this.preparateObject()).then(
      profileType => {
        if ((<any>profileType).exception)
          this.errorValidation(profileType);
        else this.callbackInsertOrUpdate();
      }, (response: Response) => {
        this.errorValidation(response);
      }
    );
  }

  findById(id: any){
    return this.profileTypeService.findById(id).then(
      profileType => {
        this.profileType = profileType;
        console.log(this.profileType);
        console.log(this.listCategory);
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
        console.log(error);
      }
    );
  }

}
