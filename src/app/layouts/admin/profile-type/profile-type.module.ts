import { NgModule } from '@angular/core';
import { AppCommonModule } from './../../../common/app-common.module';
import { ProfileTypeFormComponent } from './profile-type-form/profile-type-form.component';
import { ProfileTypeListComponent } from './profile-type-list/profile-type-list.component';
import { RouterModule } from '@angular/router';
import { profileTypeRoutes } from './profile-type.routing';
import { ProfileTypeService } from './service/profile-type.service';
import { NgxSmartModalModule } from 'ngx-smart-modal';

@NgModule({
  imports: [
    AppCommonModule,
    RouterModule.forChild(profileTypeRoutes),
    NgxSmartModalModule.forRoot()
  ],
  declarations: [ProfileTypeFormComponent, 
    ProfileTypeListComponent],
  providers:[
    ProfileTypeService
  ]
})
export class ProfileTypeModule { }
