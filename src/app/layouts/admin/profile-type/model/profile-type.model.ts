import { Basic } from "../../models/basic.model";

export class ProfileType extends Basic {
    name: string;
    category: string;
    description: string;
}