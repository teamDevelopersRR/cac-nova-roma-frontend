import { ProfileTypeListComponent } from './profile-type-list/profile-type-list.component';
import { ProfileTypeFormComponent } from './profile-type-form/profile-type-form.component';
import { Routes } from '@angular/router';
import { AuthGuard } from '../../../common/guards/auth.guard';

export const profileTypeRoutes: Routes = [
    {
        path: '',
        component: ProfileTypeListComponent,
        data: {
            breadcrumb: 'breadcrumb.profileTypes',
            permissions: ['ROLE_PROFILE_TYPE_READ']
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'edit/:id',
        component: ProfileTypeFormComponent,
        data: {
            breadcrumb: 'breadcrumb.profileTypeEdit',
            permissions: ['ROLE_PROFILE_TYPE_UPDATE']
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'new',
        component: ProfileTypeFormComponent,
        data: {
            breadcrumb: 'breadcrumb.profileTypeNew',
            permissions: ['ROLE_PROFILE_TYPE_INSERT']
        },
        canActivate: [AuthGuard]
    }
];