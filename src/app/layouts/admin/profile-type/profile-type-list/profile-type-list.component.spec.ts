import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {} from 'jasmine';

import { ProfileTypeListComponent } from './profile-type-list.component';

describe('ProfileTypeListComponent', () => {
  let component: ProfileTypeListComponent;
  let fixture: ComponentFixture<ProfileTypeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileTypeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
