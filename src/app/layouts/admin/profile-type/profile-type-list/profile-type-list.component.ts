import { Component } from '@angular/core';
import { List } from '../../list';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProfileTypeService } from '../service/profile-type.service';
import { AlertCenterService } from '../../../../common/shared/components/alert-center/service/alert-center.service';
import { TITLE_ERROR, MESSAGE_OPERATION_ERROR, TITLE_SUCCESS, MESSAGE_OPERATION_SUCCESS } from '../../../../util/ConstantProject';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { ProfileType } from '../model/profile-type.model';

@Component({
  selector: 'app-profile-type-list',
  templateUrl: './profile-type-list.component.html',
  styleUrls: ['./profile-type-list.component.scss']
})
export class ProfileTypeListComponent extends List {

  constructor(public translate: TranslateService,
    public router: Router,
    public activeRoute: ActivatedRoute,
    private profileTypeService: ProfileTypeService,
    public alertCenterService:AlertCenterService,
    public ngxSmartModalService: NgxSmartModalService) {
    super(translate, router, activeRoute, "profile-type", alertCenterService, ngxSmartModalService);
  }

  dataProvider = [];
  selectedProfileType: ProfileType = new ProfileType();

  ngOnInit() {
    this.loadDataProvider();
  }

  async loadDataProvider() {
    this.dataProvider = [];
    await this.profileTypeService.getAll().then((item) => this.dataProvider.push(item));
    this.isLoaded = true;
  }

  edit(item: any): void {
    this.router.navigate([this.routeEdit+item.id]);
  }

  confirmDelete(item){
    this.profileTypeService.delete(item).then(
      item => {
        this.loadDataProvider();
        this.alertCenterService.alertSuccess(TITLE_SUCCESS, MESSAGE_OPERATION_SUCCESS);
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
        console.log(error);
      }
    );
  }

  openModalDetails(item){
    this.selectedProfileType = item;
    this.openModal();
  }

}
