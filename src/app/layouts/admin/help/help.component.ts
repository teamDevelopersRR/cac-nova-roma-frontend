import { Component, OnInit } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

@Component({
	selector: 'app-help',
	templateUrl: './help.component.html',
	styleUrls: ['./help.component.scss'],
})
export class HelpComponent implements OnInit{

	questions = [];

	constructor(public translate: TranslateService) {}

	ngOnInit() {
		this.search();

		this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
            this.search();
        });
	}

	search(){
		let questionTranslate = [];
		this.questions = [];

		this.translate.get("manual_user").subscribe((res: string) => {
			questionTranslate = Object.keys(res).map(function (personNamedIndex) {
				let item = res[personNamedIndex];
				return item;
			});
		});

		for (let i: number = 0; i < questionTranslate.length; i += 2) {
			this.questions.push({ question: questionTranslate[i], answer: questionTranslate[i + 1] });
		}
	}

}