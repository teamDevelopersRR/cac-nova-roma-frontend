import { Routes } from '@angular/router';
import {HelpComponent} from './help.component';

export const helpRoutes: Routes = [{
    path: '',
    component: HelpComponent,
    data: {
        breadcrumb: 'breadcrumb.manualUse'
    }
}];
