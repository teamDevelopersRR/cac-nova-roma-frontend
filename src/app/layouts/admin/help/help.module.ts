import { NgModule } from '@angular/core';
import { HelpComponent } from './help.component';
import { helpRoutes } from './help.routing';
import { RouterModule } from '@angular/router';
import { AppCommonModule } from '../../../common/app-common.module';

@NgModule({
  imports: [
    AppCommonModule,
    RouterModule.forChild(helpRoutes),
  ],
  declarations: [HelpComponent]
})
export class HelpModule { }
