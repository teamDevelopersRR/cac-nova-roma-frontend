import { ActivityHistory } from "./../model/activity-history.model";
import { AuthenticationService } from "./../../../../common/authentication/authentication.service";
import {
  PROFILE_CATEGORY_STUDENT
} from "./../../../../util/ConstantProject";
import { UserService } from "./../../users/service/user.service";
import { User } from "./../../users/model/user.model";
import { Activity } from "./../model/activity.model";
import { Component, OnInit } from "@angular/core";
import { ActivityService } from "../service/activity.service";
import { Router, ActivatedRoute } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { List } from "../../list";
import { AlertCenterService } from "../../../../common/shared/components/alert-center/service/alert-center.service";
import {
  TITLE_SUCCESS,
  TITLE_ERROR,
  MESSAGE_OPERATION_ERROR,
  MESSAGE_OPERATION_SUCCESS
} from "../../../../util/ConstantProject";
import { NgxSmartModalService } from "ngx-smart-modal";
import { ActivityFile } from "../model/activity.file.model";
import { ActivityType } from "../../activity-type/model/activity-type.model";

@Component({
  selector: "app-activity-list",
  templateUrl: "./activity-list.component.html",
  styleUrls: ["./activity-list.component.scss"]
})
export class ActivityListComponent extends List implements OnInit {
  dataProvider: Activity[] = [];
  selectedActivity: Activity = new Activity();
  selectedActivityHistory: ActivityHistory[];

  loggedUser = this.authenticationService.getLoggedUser();

  students: User[] = [];
  selectedStudent: User;
  isStudent = false;
  isFiltered = false;

  constructor(
    public translate: TranslateService,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public activityService: ActivityService,
    public alertCenterService: AlertCenterService,
    private userService: UserService,
    private authenticationService: AuthenticationService,
    public ngxSmartModalService: NgxSmartModalService
  ) {
    super(
      translate,
      router,
      activeRoute,
      "activity",
      alertCenterService,
      ngxSmartModalService
    );
  }

  async ngOnInit() {
    this.selectedStudent = new User();
    this.selectedActivity.activityType = new ActivityType();

    this.isLoaded = true;
    this.isFiltered = true;

    // Carregar as atividade somente se o TIPO DE PERFIL logado é ESTUDANTE
    // Caso não, tem permissão para ver todas as atividades.
    if (this.loggedUser.typeProfile === PROFILE_CATEGORY_STUDENT) {
      this.selectedStudent.id = this.loggedUser.id;
      this.selectedStudent.login = this.loggedUser.login;
      this.selectedStudent.name = this.loggedUser.name;
      this.isStudent = true;
      this.loadDataProvider();
    }

    if (this.loggedUser.typeProfile !== PROFILE_CATEGORY_STUDENT) {
      this.activeRoute.queryParams.subscribe(params => {
        const id = params["data"];
        if (id !== undefined) {
          this.userService.findById(id).then(user => {
            this.selectedStudent = user;
            this.loadDataProvider();
          });
        }
      });
    }
  }

  async loadDataProvider() {
    this.dataProvider = [];
    this.isLoaded = false;
    await this.activityService
      .findByStudentIdAndActiveTrue(this.selectedStudent.id)
      .then(items => (this.dataProvider = items));
    this.isLoaded = true;
  }

  edit(item) {
    this.router.navigate([this.routeEdit + item.id]);
  }

  newActivity() {
    if (this.selectedStudent && this.selectedStudent.id) {
      this.router.navigate([this.route + "/new/" + this.selectedStudent.id]);
    } else {
      this.router.navigate([this.route + "/new"]);
    }
  }

  filterItemsOfType(type = '') {
    if (this.dataProvider && this.dataProvider.length > 0) {
      return this.dataProvider.filter(
        (item: Activity) => {
          if (type !== '') {
            return item.activityType.category === type;
          }
          return true;
        }
      );
    }
    else return [];
  }

  filterItemsByTypeAndPending(type) {
    if (this.dataProvider && this.dataProvider.length > 0)
      return this.dataProvider.filter(
        (item: Activity) =>
          item.activityType.category == type && item.status == "PENDING"
      );
    else return [];
  }

  get pendingActivityHour() {
    return this.filterItemsByTypeAndPending("HOUR").length;
  }

  get pendingActivityOccurrence() {
    return this.filterItemsByTypeAndPending("OCCURRENCE").length;
  }

  get pendingActivity() {
    if (this.dataProvider && this.dataProvider.length > 0)
      return this.dataProvider.filter(
        (item: Activity) => item.status == "PENDING"
      ).length;
    else return 0;
  }

  sumByTypeAndProperty(type, property) {
    const listFilter = this.filterItemsOfType(type);
    if (listFilter && listFilter.length > 0)
      return listFilter
        .map((item: Activity) => item[property])
        .reduce((previous, current, index, array) => {
          return previous + current;
        });
  }

  confirmDelete(item) {
    this.activityService.delete(item).then(
      item => {
        this.loadDataProvider();
        this.alertCenterService.alertSuccess(
          TITLE_SUCCESS,
          MESSAGE_OPERATION_SUCCESS
        );
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(
          TITLE_ERROR,
          MESSAGE_OPERATION_ERROR
        );
        console.log(error);
      }
    );
  }

  async findStudents() {
    this.isFiltered = false;
    await this.userService
      .findByLoginContains(this.selectedStudent.login, PROFILE_CATEGORY_STUDENT)
      .then(data => {
        this.isFiltered = true;
        this.students = data.body;
      });
  }

  selectStudent(event) {
    if (event.item) {
      this.selectedStudent = event.item;
      this.loadDataProvider();
    }
  }

  getDirectoryImageByExtension(type) {
    if (type != "")
      return this.util.getDirectoryImageByExtension(type.split("/")[1], 128);
    return this.util.getDirectoryImageByExtension("file", 128);
  }

  download(activityFile: ActivityFile) {
    this.util.download(activityFile.file, activityFile.name);
  }

  openNewTab(url: string) {
    window.open(url, "_blank");
  }

  openModalDetails(item) {
    this.selectedActivity = item;
    this.activityService
      .selectHistoryActivity(item.id)
      .then(
        activitiyHistory => (this.selectedActivityHistory = activitiyHistory)
      );
    this.openModal();
  }

  approveOrRejectActivity(selectedActivity: Activity, approve = true) {
    let index = this.dataProvider.indexOf(selectedActivity);
    this.activityService
      .approveOrRejectActivity(selectedActivity.id, approve)
      .then(activity => {
        if (activity) {
          this.selectedActivity = activity;
          if (index > -1) {
            this.dataProvider[index] = activity;
          }
          if (approve) {
            this.alertCenterService.alertSuccess(TITLE_SUCCESS, "alert.success.approvedSuccess");
          } else {
            this.alertCenterService.alertSuccess(TITLE_SUCCESS, "alert.success.rejectedSuccess");
          }

          this.closeModal();

          this.activityService
            .selectHistoryActivity(activity.id)
            .then(
              activitiyHistory =>
                (this.selectedActivityHistory = activitiyHistory)
            );
        }
      });
  }

  getStatus(status) {
    return status.toLowerCase();
  }

  sumByGeneral() {
    return this.sumByTypeAndProperty('', "numberHourAccounted");
  }
}
