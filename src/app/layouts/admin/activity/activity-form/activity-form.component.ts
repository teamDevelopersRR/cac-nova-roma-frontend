import {
  PROFILE_CATEGORY_STUDENT,
  TITLE_WARNING
} from "./../../../../util/ConstantProject";
import { ErrorValidationService } from "./../../../../common/shared/externals/forms/field-error/service/error-validation.service";
import { UserService } from "./../../users/service/user.service";
import { AuthenticationService } from "./../../../../common/authentication/authentication.service";
import { User } from "./../../users/model/user.model";
import { ActivityTypeService } from "./../../activity-type/service/activity-type.service";
import { ActivityService } from "./../service/activity.service";
import { Activity } from "./../model/activity.model";
import { Router, ActivatedRoute } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { Component, ViewChild } from "@angular/core";
import { Form } from "../../form";
import { IOption } from "../../../../common/shared/externals/forms/form.component";
import { Util } from "../../../../util/util";
import {
  TITLE_ERROR,
  MESSAGE_OPERATION_ERROR
} from "../../../../util/ConstantProject";
import { AlertCenterService } from "../../../../common/shared/components/alert-center/service/alert-center.service";
import { FileUploadComponent } from "../../../../common/shared/components/file-upload/file-upload.component";

@Component({
  selector: "app-activity-form",
  templateUrl: "./activity-form.component.html",
  styleUrls: ["./activity-form.component.scss"]
})
export class ActivityFormComponent extends Form {
  idUserSelected;

  constructor(
    public translate: TranslateService,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public errorValidationService: ErrorValidationService,
    public alertCenterService: AlertCenterService,
    private activityService: ActivityService,
    private activityTypeService: ActivityTypeService,
    private userService: UserService,
    private authenticationService: AuthenticationService
  ) {
    super(
      translate,
      router,
      activeRoute,
      "activity",
      errorValidationService,
      alertCenterService
    );
  }

  public activity: Activity = new Activity();
  listActivityType: IOption[] = [];
  isStudent = false;

  students: User[] = [];
  isFiltered = false;
  selectedStudent: User;

  util: Util = new Util();

  @ViewChild("fileComponent") fileComponent: FileUploadComponent;

  ngOnInit(): void {
    
    this.isFiltered = true;

    // Verificar Editar ou Inserir
    if (!this.checkEdit()) {
      this.loadLists();
      if (
        this.authenticationService.getLoggedUser().typeProfile ==
        PROFILE_CATEGORY_STUDENT
      ) {
        this.userService
          .findById(this.authenticationService.getLoggedUser().id)
          .then((user: User) => {
            this.isStudent = true;
            this.activity.student = user;
          });
      } else if (this.checkUserLoad()) {
        this.userService.findById(this.idUserSelected).then((user: User) => {
          this.activity.student = user;
        });
      } else {
        this.activity.student = new User();
      }
      this.activity.activityFiles = [];
    }
  }

  checkUserLoad() {
    this.activeRoute.params.subscribe(params => {
      const id = params["studentId"];
      if (id !== undefined) {
        this.idUserSelected = id;
      }
    });
    return this.idUserSelected;
  }

  loadLists() {
    this.activityTypeService.getDto().then(
      activityTypes => {
        this.listActivityType = this.modelListToIOptions(activityTypes, "name");
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(
          TITLE_ERROR,
          MESSAGE_OPERATION_ERROR
        );
        console.log(error);
      }
    );
  }

  insert(): void {
    if (!this.isValid()) {
      return;
    }

    this.activityService.insert(this.preparateObject()).then(
      activity => {
        if ((<any>activity).exception) this.errorValidation(activity);
        else this.callbackInsertOrUpdate(`${this.activity.student.id}`);
      },
      (response: Response) => {
        this.errorValidation(response);
      }
    );
  }

  update(): void {
    if (!this.isValid()) {
      return;
    }

    this.activityService.update(this.preparateObject()).then(
      activity => {
        if ((<any>activity).exception) this.errorValidation(activity);
        else this.callbackInsertOrUpdate(`${this.activity.student.id}`);
      },
      (response: Response) => {
        this.errorValidation(response);
      }
    );
  }

  isValid() {
    if (this.activity.dateActivity instanceof Date) {
      this.alertCenterService.alertWarning(
        TITLE_WARNING,
        "errors.activity.dateActivity.empty"
      );
      return false;
    }
    return true;
  }

  preparateObject() {
    this.isLoading = true;
    const copy = Object.assign({}, this.activity);
    // RETIRAR MÁSCARAS / MODIFICAR VALORES
    return copy;
  }

  findById(id: any) {
    return this.activityService.findById(id).then(
      activity => {
        this.activity = activity;
        this.loadLists();
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(
          TITLE_ERROR,
          MESSAGE_OPERATION_ERROR
        );
        console.log(error);
      }
    );
  }

  async findStudents() {
    this.selectedStudent = new User();
    this.isFiltered = false;
    await this.userService
      .findByLoginContains(
        this.activity.student.login,
        PROFILE_CATEGORY_STUDENT
      )
      .then(data => {
        this.isFiltered = true;
        this.students = data.body;
      });
  }

  selectStudent(event) {
    if (event.item) {
      this.activity.student = event.item;
      this.selectedStudent = event.item;

      this.removeErrors(event.control.name);
    }
  }

  changeInput(field, $event) {
    switch (field) {
      case "numberHour":
        this.activity.numberHour = parseInt(this.activity.numberHour + "");
        break;
      case "numberOccurrence":
        this.activity.numberOccurrence = parseInt(
          this.activity.numberOccurrence + ""
        );
        break;
    }
  }

  onFileChange(files: FileList) {
    for (var i = 0; i < files.length; i++) {
      const reader = new FileReader();
      const file: File = files[i];
      reader.readAsDataURL(file);
      reader.onload = event => {
        if (reader.result) {
          this.activity.activityFiles.push({
            name: file.name,
            type: file.type,
            size: file.size,
            file: reader.result.split(",")[1]
          });
        }
      };
    }

    console.log(this.activity.activityFiles);
  }

  getDirectoryImageByExtension(type) {
    if (type != "")
      return this.util.getDirectoryImageByExtension(type.split("/")[1], 64);
    return this.util.getDirectoryImageByExtension("file", 64);
  }

  deleteFile(i) {
    this.activity.activityFiles.splice(i, 1);
  }

  comebackActivity() {
    if (this.activity.student && this.activity.student.id)
      this.comeback(`${this.activity.student.id}`);
    else this.comeback();
  }
}
