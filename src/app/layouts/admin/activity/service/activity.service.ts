import { ActivityHistory } from './../model/activity-history.model';
import { Observable } from 'rxjs/Rx';
import { Activity } from './../model/activity.model';
import { HttpClient, HttpRequest, HttpEvent, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';

@Injectable()
export class ActivityService {

  constructor(private http: HttpClient) { }

  async insert(activity: Activity) {
    return await this.http.post<Activity>(`${environment.url_application}/activity/`, activity).toPromise();
  }

  async update(activity: Activity) {
    return await this.http.put<Activity>(`${environment.url_application}/activity/`, activity).toPromise();
  }

  async delete(activity: Activity) {
    return await this.http.delete<Activity>(`${environment.url_application}/activity/${activity.id}`).toPromise();
  }

  async getAllByActiveTrue() {
    return await this.http.get<Activity[]>(`${environment.url_application}/activity/`).toPromise();
  }

  async findById(id: number) {
    return await this.http.get<Activity>(`${environment.url_application}/activity/${id}`).toPromise();
  }

  async findByStudentIdAndActiveTrue(studentId) {
    return await this.http.get<Activity[]>(`${environment.url_application}/activity/findByStudentId/${studentId}`).toPromise();
  }

  async pushFileToStorage(file: File) {
    let formdata: FormData = new FormData();
    formdata.append('file', file);
    return await this.http.post<string>(`${environment.url_application}/activity/file`, formdata,
      {
        observe: 'response'
      }).toPromise();
  }

  async approveOrRejectActivity(activityId: number, approve = false) {
    return await this.http.post<Activity>(`${environment.url_application}/activity/approveOrReject/${activityId}/${approve}`, null).toPromise();
  }

  async selectHistoryActivity(activityId: number) {
    return await this.http.get<ActivityHistory[]>(`${environment.url_application}/activity/activityHistory/${activityId}`).toPromise();
  }
}