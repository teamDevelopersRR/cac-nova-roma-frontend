import { ActivityListComponent } from './activity-list/activity-list.component';
import { ActivityFormComponent } from './activity-form/activity-form.component';
import { Routes } from '@angular/router';
import { AuthGuard } from '../../../common/guards/auth.guard';

export const activityRoutes: Routes = [
    {
        path: '',
        component: ActivityListComponent,
        data: {
            breadcrumb: 'breadcrumb.activities',
            permissions: ['ROLE_ACTIVITY_READ']
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'edit/:id',
        component: ActivityFormComponent,
        data: {
            breadcrumb: 'breadcrumb.activityEdit',
            permissions: ['ROLE_ACTIVITY_UPDATE']
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'new',
        component: ActivityFormComponent,
        data: {
            breadcrumb: 'breadcrumb.activityNew',
            permissions: ['ROLE_ACTIVITY_INSERT']
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'new/:studentId',
        component: ActivityFormComponent,
        data: {
            breadcrumb: 'breadcrumb.activityNew',
            permissions: ['ROLE_ACTIVITY_INSERT']
        },
        canActivate: [AuthGuard]
    }
];