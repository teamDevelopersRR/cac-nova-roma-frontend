import { UserService } from './../users/service/user.service';
import { ActivityTypeService } from './../activity-type/service/activity-type.service';
import { ActivityService } from './service/activity.service';
import { RouterModule } from '@angular/router';
import { AppCommonModule } from './../../../common/app-common.module';
import { NgModule } from '@angular/core';
import { ActivityFormComponent } from './activity-form/activity-form.component';
import { ActivityListComponent } from './activity-list/activity-list.component';
import { activityRoutes } from './activity.routing';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgxSmartModalModule } from 'ngx-smart-modal';

@NgModule({
  imports: [
    AppCommonModule,
    RouterModule.forChild(activityRoutes),
    NgxSmartModalModule.forRoot()
  ],
  declarations: [
    ActivityFormComponent,
    ActivityListComponent
  ],
  providers: [
    ActivityService,
    ActivityTypeService,
    UserService,
    NgbDatepickerConfig
  ]
})
export class ActivityModule { }