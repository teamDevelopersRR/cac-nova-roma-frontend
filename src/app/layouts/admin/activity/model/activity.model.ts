import { ActivityFile } from './activity.file.model';
import { User } from './../../users/model/user.model';
import { ActivityType } from './../../activity-type/model/activity-type.model';
import { Basic } from '../../models/basic.model';

export class Activity extends Basic {
    name: string;
    active: boolean;
    dateActivity: Date = new Date();
    numberOccurrence?: number;
    numberOccurrenceAccounted?: number;
    numberOccurrenceGlosa?: number;
    numberHour?: number;
    numberHourAccounted?: number;
    numberHourGlosa?: number;
    typeAppropiation: any;
    status: string;
    student: User = new User();
    activityType?: ActivityType;
    activityFiles?: ActivityFile[];
}