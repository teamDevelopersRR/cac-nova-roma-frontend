import { Activity } from './activity.model';
import { Basic } from './../../models/basic.model';
export class ActivityHistory extends Basic {
    statusFrom: string;
    statusTo: string;
    activity: Activity;
}
