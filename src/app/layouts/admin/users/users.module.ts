import { RouterModule } from '@angular/router';
import { AppCommonModule } from './../../../common/app-common.module';
import { NgModule } from '@angular/core';
import { UsersFormComponent } from './users-form/users-form.component';
import { UsersListComponent } from './users-list/users-list.component';
import { usersRoutes } from './users.routing';
import { UserService } from './service/user.service';
import { CourseService } from '../course/service/course.service';
import { ProfileService } from '../profile/service/profile.service';
import { NgxSmartModalModule } from 'ngx-smart-modal';

@NgModule({
  imports: [
    AppCommonModule,
    RouterModule.forChild(usersRoutes),
    NgxSmartModalModule.forRoot()
  ],
  declarations: [
    UsersFormComponent,
    UsersListComponent
  ],
  providers: [UserService, CourseService, ProfileService]
})
export class UsersModule { }