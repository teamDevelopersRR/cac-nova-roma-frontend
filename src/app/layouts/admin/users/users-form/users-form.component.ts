import { ErrorValidationService } from './../../../../common/shared/externals/forms/field-error/service/error-validation.service';
import { User } from './../model/user.model';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { Form } from '../../form';
import { UserService } from '../service/user.service';
import { Course } from '../../course/model/course.model';
import { CourseService } from '../../course/service/course.service';
import { Profile } from '../../profile/model/profile.model';
import { ProfileService } from '../../profile/service/profile.service';
import { AlertCenterService } from '../../../../common/shared/components/alert-center/service/alert-center.service';
import { TITLE_ERROR, MESSAGE_OPERATION_ERROR } from '../../../../util/ConstantProject';

@Component({
  selector: 'app-users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.scss']
})
export class UsersFormComponent extends Form implements OnInit {

  constructor(public translate: TranslateService,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public errorValidationService: ErrorValidationService,
    public alertCenterService: AlertCenterService,
    public userService: UserService,
    public courseService: CourseService,
    public profileService: ProfileService) {
    super(translate, router, activeRoute, 'users', errorValidationService, alertCenterService);
  }

  public user: User = new User;
  course: Course = new Course();
  courses = [];
  profile: Profile = new Profile();
  profiles = [];
  isEdit = false;
  isFilteredProfile = false;
  isFilteredCourse = false;

  ngOnInit() {
    this.isFilteredProfile = true;
    this.isFilteredCourse = true;
    
    if (!this.checkEdit()) {

    }
  }

  preparateObject() {
    this.isLoading = true;
    const copy = Object.assign({}, this.user);
    return copy;
  }

  insert(): void {

    if(this.user.id){
      this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
      return;
    }

    this.user.course = this.course;
    this.user.profile = this.profile;

    this.userService.insert(this.preparateObject()).then(
      user => {
        if ((<any>user).exception)
          this.errorValidation(user);
        else this.callbackInsertOrUpdate();
      }, (response: Response) => {
        this.errorValidation(response);
      }
    );
  }

  update(): void {
    this.user.course = this.course;
    this.user.profile = this.profile;

    this.userService.update(this.preparateObject()).then(
      user => {
        if ((<any>user).exception)
          this.errorValidation(user);
        else this.callbackInsertOrUpdate();
      }, (response: Response) => {
        this.errorValidation(response);
      }
    );
  }

  findById(id: any) {
    return this.userService.findById(id).then(
      user => {
        this.user = user;

        this.isEdit = true;
      
        this.findProfileById(this.user.profile.id);
        this.findCourseById(this.user.course.id);
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
        console.log(error);
      }
    );
  }

  async findCourse() {
    this.isFilteredCourse = false;
    await this.courseService.findByName(this.course.name)
      .then(data => {
        this.isFilteredCourse = true;
        this.courses = data.body;
      });
  }

  selectCourse(event) {
    if (event.item) {
      this.course = event.item;
    }
  }

  findCourseById(id: any) {
    return this.courseService.findById(id).then(
      course => {
        this.course = course;
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
        console.log(error);
      }
    );
  }

  async findProfile() {
    this.isFilteredProfile = false;
    await this.profileService.findByName(this.profile.name)
      .then(data => {
        this.isFilteredProfile = true;
        this.profiles = data.body;
      });
  }

  selectProfile(event) {
    if (event.item) {
      this.profile = event.item;
    }
  }

  findProfileById(id: any) {
    return this.profileService.findById(id).then(
      profile => {
        this.profile = profile;
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
        console.log(error);
      }
    );
  }

}
