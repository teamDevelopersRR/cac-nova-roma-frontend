import { UsersListComponent } from './users-list/users-list.component';
import { UsersFormComponent } from './users-form/users-form.component';
import { Routes } from '@angular/router';
import { AuthGuard } from '../../../common/guards/auth.guard';

export const usersRoutes: Routes = [
    {
        path: '',
        component: UsersListComponent,
        data: {
            breadcrumb: 'breadcrumb.users',
            permissions: ['ROLE_USER_READ']
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'edit/:id',
        component: UsersFormComponent,
        data: {
            breadcrumb: 'breadcrumb.userEdit',
            permissions: ['ROLE_USER_UPDATE']
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'new',
        component: UsersFormComponent,
        data: {
            breadcrumb: 'breadcrumb.userNew',
            permissions: ['ROLE_USER_INSERT']
        },
        canActivate: [AuthGuard]
    }
];