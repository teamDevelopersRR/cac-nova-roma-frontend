import { User } from './../model/user.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';

@Injectable()
export class UserService {

    constructor(private http: HttpClient) { }

    async insert(user: User) {
        return await this.http.post<User>(`${environment.url_application}/user/`, user).toPromise();
    }

    async update(user: User) {
        return await this.http.put<User>(`${environment.url_application}/user/`, user).toPromise();
    }
  
    async delete(user: User) {
      return await this.http.delete<User>(`${environment.url_application}/user/${user.id}`).toPromise();
    }

    async getAll() {
        return await this.http.get<User[]>(`${environment.url_application}/user/`).toPromise();
    }

    async findById(id: number) {
        return await this.http.get<User>(`${environment.url_application}/user/${id}`).toPromise();
    }

    async findByLoginContains(login, profileType) {
        const params = new HttpParams()
            .set('login', login)
            .set('typeProfile', profileType);
        return await this.http.post<User[]>(`${environment.url_application}/user/findByLoginContains?`, null,
            {
                params: params,
                observe: 'response'
            }
        ).toPromise();
    }
}