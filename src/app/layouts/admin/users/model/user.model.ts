import { Course } from './../../course/model/course.model';
import { Profile } from './../../profile/model/profile.model';
import { Basic } from '../../models/basic.model';

export class User extends Basic{
    name: string;
    login: string;
    password: string;
    cpf: string;
    course: Course = new Course();
    profile: Profile = new Profile();
    ckUpdatePassword: string;
}