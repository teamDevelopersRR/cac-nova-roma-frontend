import { DisciplineListComponent } from './discipline-list/discipline-list.component';
import { DisciplineFormComponent } from './discipline-form/discipline-form.component';
import { Routes } from '@angular/router';
import { AuthGuard } from '../../../common/guards/auth.guard';

export const disciplineRoutes: Routes = [
    {
        path: '',
        component: DisciplineListComponent,
        data: {
            breadcrumb: 'breadcrumb.disciplines',
            permissions: ['ROLE_DISCIPLINE_READ']
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'edit/:id',
        component: DisciplineFormComponent,
        data: {
            breadcrumb: 'breadcrumb.disciplineEdit',
            permissions: ['ROLE_DISCIPLINE_UPDATE']
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'new',
        component: DisciplineFormComponent,
        data: {
            breadcrumb: 'breadcrumb.disciplineNew',
            permissions: ['ROLE_DISCIPLINE_INSERT']
        },
        canActivate: [AuthGuard]
    }
];