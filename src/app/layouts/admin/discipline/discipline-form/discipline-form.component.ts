import { ErrorValidationService } from './../../../../common/shared/externals/forms/field-error/service/error-validation.service';
import { Discipline } from './../model/discipline.model';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { Form } from '../../form';
import { DisciplineService } from '../service/discipline.service';
import { AlertCenterService } from '../../../../common/shared/components/alert-center/service/alert-center.service';
import { MESSAGE_OPERATION_ERROR, TITLE_ERROR } from '../../../../util/ConstantProject';

@Component({
  selector: 'app-discipline-form',
  templateUrl: './discipline-form.component.html',
  styleUrls: ['./discipline-form.component.scss']
})
export class DisciplineFormComponent extends Form implements OnInit {

  constructor(public translate: TranslateService,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public errorValidationService: ErrorValidationService,
    public alertCenterService: AlertCenterService,
    public disciplineService: DisciplineService) {
    super(translate, router, activeRoute, 'discipline', errorValidationService, alertCenterService);
  }

  discipline: Discipline = new Discipline;

  ngOnInit() {
    if (!this.checkEdit()) {

    }
  }

  preparateObject() {
    this.isLoading = true;
    const copy = Object.assign({}, this.discipline);
    return copy;
  }

  insert(): void {
    this.disciplineService.insert(this.preparateObject()).then(
      discipline => {
        if ((<any>discipline).exception) this.errorValidation(discipline);
        else this.callbackInsertOrUpdate();
      }, (response: Response) => {
        this.errorValidation(response);
      }
    );
  }

  update(): void {
    this.disciplineService.update(this.preparateObject()).then(
      discipline => {
        if ((<any>discipline).exception) this.errorValidation(discipline);
        else this.callbackInsertOrUpdate();
      }, (response: Response) => {
        this.errorValidation(response);
      }
    );
  }

  findById(id: any) {
    return this.disciplineService.findById(id).then(
      discipline => {
        this.discipline = discipline;
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
        console.log(error);
      }
    );
  }

  changeInput(field, $event) {
    switch (field) {
      case 'numberHour':
        this.discipline.numberHour = parseInt(this.discipline.numberHour + "");
        break;
    }
  }

}
