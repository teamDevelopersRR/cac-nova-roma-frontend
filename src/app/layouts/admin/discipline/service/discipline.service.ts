import { Discipline } from './../model/discipline.model';
import { HttpClient, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';

@Injectable()
export class DisciplineService {

  constructor(private http: HttpClient) { }

  async insert(discipline: Discipline) {
    return await this.http.post<Discipline>(`${environment.url_application}/discipline/`, discipline).toPromise();
  }

  async update(discipline: Discipline) {
    return await this.http.put<Discipline>(`${environment.url_application}/discipline/`, discipline).toPromise();
  }
  
  async delete(discipline: Discipline) {
    return await this.http.delete<Discipline>(`${environment.url_application}/discipline/${discipline.id}`).toPromise();
  }

  async getAll() {
    return await this.http.get<Discipline[]>(`${environment.url_application}/discipline/`).toPromise();
  }

  async findById(id: number) {
    return await this.http.get<Discipline>(`${environment.url_application}/discipline/${id}`).toPromise();
  }

  async findByName(name) {
    const params = new HttpParams().set('name', name);

      return await this.http.post<Discipline[]>(`${environment.url_application}/discipline/getByName?`, null,
          {
              params: params,
              observe: 'response'
          }
      ).toPromise();
  }

}
