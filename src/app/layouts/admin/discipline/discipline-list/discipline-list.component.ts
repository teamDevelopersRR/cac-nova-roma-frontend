import { Component, OnInit } from '@angular/core';
import { DisciplineService } from '../service/discipline.service';
import { Router, ActivatedRoute } from '@angular/router';
import { List } from '../../list';
import { TranslateService } from '@ngx-translate/core';
import { AlertCenterService } from '../../../../common/shared/components/alert-center/service/alert-center.service';
import { TITLE_ERROR, MESSAGE_OPERATION_ERROR, TITLE_SUCCESS, MESSAGE_OPERATION_SUCCESS } from '../../../../util/ConstantProject';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Discipline } from '../model/discipline.model';

@Component({
  selector: 'app-discipline-list',
  templateUrl: './discipline-list.component.html',
  styleUrls: ['./discipline-list.component.scss']
})
export class DisciplineListComponent extends List implements OnInit {

  dataProvider = [];
  selectedDiscipline: Discipline = new Discipline();

  constructor(public translate: TranslateService,
    public router: Router,
    public activeRoute: ActivatedRoute,
    private disciplineService: DisciplineService,
    public alertCenterService:AlertCenterService,
    public ngxSmartModalService: NgxSmartModalService) {
    super(translate, router, activeRoute, "discipline", alertCenterService, ngxSmartModalService);
  }

  ngOnInit() {
    this.loadDataProvider();
  }

  async loadDataProvider() {
    this.dataProvider = [];
    await this.disciplineService.getAll().then((item) => this.dataProvider.push(item));
    this.isLoaded = true;
  }

  edit(item){
    this.router.navigate([this.routeEdit+item.id]);
  }

  confirmDelete(item){
    this.disciplineService.delete(item).then(
      item => {
        this.loadDataProvider();
        this.alertCenterService.alertSuccess(TITLE_SUCCESS, MESSAGE_OPERATION_SUCCESS);
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
        console.log(error);
      }
    );
  }

  openModalDetails(item){
    this.selectedDiscipline = item;
    this.openModal();
  }

}
