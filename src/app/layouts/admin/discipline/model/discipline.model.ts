import { Basic } from "../../models/basic.model";

export class Discipline extends Basic {
    name: string;
    numberHour: number;
}