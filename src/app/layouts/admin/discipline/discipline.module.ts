import { RouterModule } from '@angular/router';
import { AppCommonModule } from './../../../common/app-common.module';
import { NgModule } from '@angular/core';
import { DisciplineFormComponent } from './discipline-form/discipline-form.component';
import { DisciplineListComponent } from './discipline-list/discipline-list.component';
import { disciplineRoutes } from './discipline.routing';
import { DisciplineService } from './service/discipline.service';
import { NgxSmartModalModule } from 'ngx-smart-modal';

@NgModule({
  imports: [
    AppCommonModule,
    RouterModule.forChild(disciplineRoutes),
    NgxSmartModalModule.forRoot()
  ],
  declarations: [
    DisciplineFormComponent,
    DisciplineListComponent
  ],
  providers: [DisciplineService]
})
export class DisciplineModule { }