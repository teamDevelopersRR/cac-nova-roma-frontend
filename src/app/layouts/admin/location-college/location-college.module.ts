import { NgModule } from '@angular/core';
import { AppCommonModule } from '../../../common/app-common.module';
import { RouterModule } from '@angular/router';
import { locationCollegeRoutes } from './location-college.routing';
import { LocationCollegeComponent } from './location-college.component';

@NgModule({
  imports: [
    AppCommonModule,
    RouterModule.forChild(locationCollegeRoutes),
  ],
  declarations: [LocationCollegeComponent]
})
export class LocationCollegeModule { }
