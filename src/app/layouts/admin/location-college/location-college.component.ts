import { Component } from '@angular/core';

@Component({
  selector: 'app-location-college',
  templateUrl: './location-college.component.html',
  styleUrls: ['./location-college.component.scss']
})
export class LocationCollegeComponent {

  zoom = 12;

  constructor() { }

}
