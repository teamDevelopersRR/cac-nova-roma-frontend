import { Routes } from '@angular/router';
import { LocationCollegeComponent } from './location-college.component';

export const locationCollegeRoutes: Routes = [{
    path: '',
    component: LocationCollegeComponent,
    data: {
        breadcrumb: 'breadcrumb.locationColleges'
    }
}];
