import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {} from 'jasmine';

import { LocationCollegeComponent } from './location-college.component';

describe('LocationCollegeComponent', () => {
  let component: LocationCollegeComponent;
  let fixture: ComponentFixture<LocationCollegeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocationCollegeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationCollegeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
