import { ActivityTypeService } from './service/activity-type.service';
import { RouterModule } from '@angular/router';
import { AppCommonModule } from './../../../common/app-common.module';
import { NgModule } from '@angular/core';
import { ActivityTypeFormComponent } from './activity-type-form/activity-type-form.component';
import { ActivityTypeListComponent } from './activity-type-list/activity-type-list.component';
import { activityTypeRoutes } from './activity-type.routing';
import { CourseService } from '../course/service/course.service';
import { ActivityTypeCourseFormComponent } from './activity-type-course-form/activity-type-course-form.component';
import { ActivityTypeCourseService } from './activity-type-course-form/service/activity-type-course.service';
import { NgxSmartModalModule } from 'ngx-smart-modal';

@NgModule({
  imports: [
    AppCommonModule,
    RouterModule.forChild(activityTypeRoutes),
    NgxSmartModalModule.forRoot()
  ],
  declarations: [
    ActivityTypeFormComponent,
    ActivityTypeListComponent,
    ActivityTypeCourseFormComponent
  ],
  providers: [ActivityTypeService, CourseService, ActivityTypeCourseService]
})
export class ActivityTypeModule { }