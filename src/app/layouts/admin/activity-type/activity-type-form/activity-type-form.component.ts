import { ErrorValidationService } from './../../../../common/shared/externals/forms/field-error/service/error-validation.service';
import { ActivityType } from './../model/activity-type.model';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Component } from '@angular/core';
import { IOption } from '../../../../common/shared/externals/forms/form.component';
import { ActivityTypeService } from '../service/activity-type.service';
import { CourseService } from '../../course/service/course.service';
import { ActivityTypeCourse } from '../activity-type-course-form/model/activity-type-course.model';
import { ActivityTypeCourseService } from '../activity-type-course-form/service/activity-type-course.service';
import { AlertCenterService } from '../../../../common/shared/components/alert-center/service/alert-center.service';
import { TITLE_ERROR, MESSAGE_OPERATION_ERROR, TITLE_SUCCESS, MESSAGE_OPERATION_SUCCESS } from '../../../../util/ConstantProject';
import { FormList } from '../../form-list';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-activity-type-form',
  templateUrl: './activity-type-form.component.html',
  styleUrls: ['./activity-type-form.component.scss']
})
export class ActivityTypeFormComponent extends FormList {

  constructor(public translate: TranslateService,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public errorValidationService: ErrorValidationService,
    public alertCenterService:AlertCenterService,
    public activityTypeService: ActivityTypeService,
    public courseService:CourseService,
    public activityTypeCourseService: ActivityTypeCourseService,
    public ngxSmartModalService: NgxSmartModalService) {
    super(translate, router, activeRoute, 'activity-type', errorValidationService, alertCenterService, ngxSmartModalService);
  }

  public activityType: ActivityType = new ActivityType();
  selectedActivityTypeCourse: ActivityTypeCourse = new ActivityTypeCourse();
  titleAba: string = "";

  dataProvider = [];

  list:IOption [] = [
    {
      id:'hour',
      label:'label.hour',
      hasLabel: true,
      value:'HOUR'
    }, 
    {
      id:'occurrence',
      label:'label.occurrence',
      hasLabel: true,
      value:'OCCURRENCE'
    }
  ]

  ngOnInit() {
    if (!this.checkEdit()) {

    }
  }

  preparateObject() {
    this.isLoading = true;
    const copy = Object.assign({}, this.activityType);
    return copy;
  }

  insert(): void {
    this.activityTypeService.insert(this.preparateObject()).then(
      activityType => {
        if ((<any>activityType).exception) this.errorValidation(activityType);
        else this.callbackInsertOrUpdate();
      }, (response: Response) => {
        this.errorValidation(response);
      }
    );
  }

  update(): void {
    this.activityTypeService.update(this.preparateObject()).then(
      activityType => {
        if ((<any>activityType).exception) this.errorValidation(activityType);
        else this.callbackInsertOrUpdate();
      }, (response: Response) => {
        this.errorValidation(response);
      }
    );
  }

  edit(item): void {
    this.router.navigate([this.routeEdit+String(this.activityType.id)+"/activity-type-course/edit/"+item.id]);
  }

  async getActivityTypeCourseByActivityType(){
    this.dataProvider = [];
    await this.activityTypeCourseService.findByActivityType(this.activityType.id).then((item) => this.dataProvider.push(item));
    this.isLoaded = true;
  }

  findById(id: any){
    return this.activityTypeService.findById(id).then(
      activityType => {
        this.activityType = activityType;
        this.titleAba = this.activityType.name;
        
        this.getActivityTypeCourseByActivityType();
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
        console.log(error);
      }
    );
  }
  
  confirmDelete(item){
    this.activityTypeCourseService.delete(item).then(
      item => {
        this.getActivityTypeCourseByActivityType();
        this.alertCenterService.alertSuccess(TITLE_SUCCESS, MESSAGE_OPERATION_SUCCESS);
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
        console.log(error);
      }
    );
  }

  openModalDetails(item){
    this.selectedActivityTypeCourse = item;
    this.openModal();
  }

}
