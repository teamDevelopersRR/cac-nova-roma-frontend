import { ActivityTypeListComponent } from './activity-type-list/activity-type-list.component';
import { ActivityTypeFormComponent } from './activity-type-form/activity-type-form.component';
import { Routes } from '@angular/router';
import { ActivityTypeCourseFormComponent } from './activity-type-course-form/activity-type-course-form.component';
import { AuthGuard } from '../../../common/guards/auth.guard';

export const activityTypeRoutes: Routes = [
    {
        path: '',
        component: ActivityTypeListComponent,
        data: {
            breadcrumb: 'breadcrumb.activityTypes',
            permissions: ['ROLE_ACTIVITY_TYPE_READ']
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'edit/:id',
        component: ActivityTypeFormComponent,
        data: {
            breadcrumb: 'breadcrumb.activityTypeEdit',
            permissions: ['ROLE_ACTIVITY_TYPE_UPDATE', 'ROLE_ACTIVITY_TYPE_COURSE_READ']
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'new',
        component: ActivityTypeFormComponent,
        data: {
            breadcrumb: 'breadcrumb.activityTypeNew',
            permissions: ['ROLE_ACTIVITY_TYPE_INSERT', 'ROLE_ACTIVITY_TYPE_COURSE_READ']
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'edit/:id-activity-type/activity-type-course/edit/:id',
        component: ActivityTypeCourseFormComponent,
        data: {
            breadcrumb: 'breadcrumb.activityTypeCourseEdit',
            permissions: ['ROLE_ACTIVITY_TYPE_COURSE_UPDATE']
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'edit/:id-activity-type/activity-type-course/new',
        component: ActivityTypeCourseFormComponent,
        data: {
            breadcrumb: 'breadcrumb.activityTypeCourseNew',
            permissions: ['ROLE_ACTIVITY_TYPE_COURSE_INSERT']
        },
        canActivate: [AuthGuard]
    }
];