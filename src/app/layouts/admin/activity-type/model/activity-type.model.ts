import { Basic } from "../../models/basic.model";
import { ActivityTypeCourse } from "../activity-type-course-form/model/activity-type-course.model";

export class ActivityType extends Basic {
    name: string;
    description?: string;
    category: string;
}
