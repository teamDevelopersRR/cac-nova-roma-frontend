/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ActivityTypeService } from './activity-type.service';

describe('Service: ActivityType', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActivityTypeService]
    });
  });

  it('should ...', inject([ActivityTypeService], (service: ActivityTypeService) => {
    expect(service).toBeTruthy();
  }));
});