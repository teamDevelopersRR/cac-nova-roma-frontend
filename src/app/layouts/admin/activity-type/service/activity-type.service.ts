import { ActivityType } from './../model/activity-type.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';

@Injectable()
export class ActivityTypeService {

    constructor(private http: HttpClient) { }

    async insert(activityType: ActivityType) {
      return await this.http.post<ActivityType>(`${environment.url_application}/activityType/`, activityType).toPromise();
    }
  
    async update(activityType: ActivityType) {
      return await this.http.put<ActivityType>(`${environment.url_application}/activityType/`, activityType).toPromise();
    }
  
    async delete(activityType: ActivityType) {
      return await this.http.delete<ActivityType>(`${environment.url_application}/activityType/${activityType.id}`).toPromise();
    }
  
    async getAll() {
      return await this.http.get<ActivityType[]>(`${environment.url_application}/activityType/`).toPromise();
    }
  
    async findById(id: number) {
      return await this.http.get<ActivityType>(`${environment.url_application}/activityType/${id}`).toPromise();
    }

    async getDto() {
      return await this.http.get<ActivityType[]>(`${environment.url_application}/activityType/dto`).toPromise();
    }
  
}