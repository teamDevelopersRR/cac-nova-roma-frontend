import { Component, OnInit } from '@angular/core';
import { ActivityTypeService } from '../service/activity-type.service';
import { ActivityType } from '../model/activity-type.model';
import { List } from '../../list';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertCenterService } from '../../../../common/shared/components/alert-center/service/alert-center.service';
import { TITLE_ERROR, MESSAGE_OPERATION_ERROR, TITLE_SUCCESS, MESSAGE_OPERATION_SUCCESS } from '../../../../util/ConstantProject';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-activity-type-list',
  templateUrl: './activity-type-list.component.html',
  styleUrls: ['./activity-type-list.component.scss']
})
export class ActivityTypeListComponent extends List implements OnInit{

  dataProvider = [];
  selectedActivityType: ActivityType = new ActivityType();

  constructor(public translate: TranslateService,
    public router: Router,
    public activeRoute: ActivatedRoute,
    private activityTypeService: ActivityTypeService,
    public alertCenterService:AlertCenterService,
    public ngxSmartModalService: NgxSmartModalService) {
    super(translate, router, activeRoute, "activity-type", alertCenterService, ngxSmartModalService);
  }

  ngOnInit() {
    this.loadDataProvider();
  }

  async loadDataProvider() {
    this.dataProvider = [];
    await this.activityTypeService.getAll().then((item) => this.dataProvider.push(item));
    this.isLoaded = true;
  }

  edit(item){
    this.router.navigate([this.routeEdit+item.id]);
  }

  confirmDelete(item){
    this.activityTypeService.delete(item).then(
      item => {
        this.loadDataProvider();
        this.alertCenterService.alertSuccess(TITLE_SUCCESS, MESSAGE_OPERATION_SUCCESS);
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
        console.log(error);
      }
    );
  }

  openModalDetails(item){
    this.selectedActivityType = item;
    this.openModal();
  }

}
