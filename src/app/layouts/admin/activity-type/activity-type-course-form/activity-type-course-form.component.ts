import { IOption } from './../../../../common/shared/externals/forms/form.component';
import { Component } from "@angular/core";
import { Course } from "../../course/model/course.model";
import { Form } from "../../form";
import { Router, ActivatedRoute } from "@angular/router";
import { ErrorValidationService } from "../../../../common/shared/externals/forms/field-error/service/error-validation.service";
import { TranslateService } from "@ngx-translate/core";
import { CourseService } from "../../course/service/course.service";
import { ActivityTypeCourse } from "./model/activity-type-course.model";
import { ActivityType } from "../model/activity-type.model";
import { ActivityTypeService } from "../service/activity-type.service";
import { ActivityTypeCourseService } from "./service/activity-type-course.service";
import { AlertCenterService } from "../../../../common/shared/components/alert-center/service/alert-center.service";
import {
  TITLE_ERROR,
  MESSAGE_OPERATION_ERROR
} from "../../../../util/ConstantProject";

@Component({
  selector: "app-activity-type-course-form",
  templateUrl: "./activity-type-course-form.component.html",
  styleUrls: ["./activity-type-course-form.component.scss"]
})
export class ActivityTypeCourseFormComponent extends Form {
  constructor(
    public translate: TranslateService,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public errorValidationService: ErrorValidationService,
    public alertCenterService: AlertCenterService,
    public courseService: CourseService,
    public activityTypeCourseService: ActivityTypeCourseService,
    public activityTypeService: ActivityTypeService
  ) {
    super(
      translate,
      router,
      activeRoute,
      "activity-type-course",
      errorValidationService,
      alertCenterService
    );
  }

  public activityTypeCourse: ActivityTypeCourse = new ActivityTypeCourse();
  public activityType: ActivityType = new ActivityType();
  private lastIndex: string = "";

  courses: IOption[] = [];

  ngOnInit() {
    this.activityTypeCourse.numberHourRelationship = 1;

    if (!this.checkEdit()) {
      this.findByActivityTypeId(
        this.activeRoute.params["_value"]["id-activity-type"]
      );
      this.activityTypeCourse.course = new Course();
    }

    this.loadLists();
  }

  loadLists() {
    this.courseService.getAll().then(courses => {
        this.courses = this.modelListToIOptions(courses, "name");
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(
          TITLE_ERROR,
          MESSAGE_OPERATION_ERROR
        );
        console.log(error);
      }
    );
  }

  preparateObject() {
    this.isLoading = true;
    const copy = Object.assign({}, this.activityTypeCourse);
    return copy;
  }

  insert(): void {
    this.activityTypeCourse.activityType = this.activityType;

    this.activityTypeCourseService.insert(this.preparateObject()).then(
        activityTypeCourse => {
          if ((<any>activityTypeCourse).exception)
            this.errorValidation(activityTypeCourse);
          else this.callbackInsertOrUpdate();
        },
        (response: Response) => {
          this.errorValidation(response);
        }
      )
      .catch(error => console.log(error));
  }

  update(): void {
    this.activityTypeCourseService.update(this.preparateObject()).then(
      activityTypeCourse => {
        if ((<any>activityTypeCourse).exception)
          this.errorValidation(activityTypeCourse);
        else this.callbackInsertOrUpdate();
      },
      (response: Response) => {
        this.errorValidation(response);
      }
    );
  }

  findById(id: any) {
    return this.activityTypeCourseService.findById(id).then(
      activityTypeCourse => {
        this.activityTypeCourse = activityTypeCourse;
        this.activityType = activityTypeCourse.activityType;
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(
          TITLE_ERROR,
          MESSAGE_OPERATION_ERROR
        );
        console.log(error);
      }
    );
  }

  findByActivityTypeId(id: any) {
    return this.activityTypeService.findById(id).then(
      activityType => {
        this.activityType = activityType;
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(
          TITLE_ERROR,
          MESSAGE_OPERATION_ERROR
        );
        console.log(error);
      }
    );
  }

  comeback() {
    this.router.navigate(["activity-type/edit/" + this.activityType.id]);
  }

  changeInput(field, $event) {
    switch (field) {
      case "semester":
        if (this.activityTypeCourse.semester) {
          this.lastIndex = this.activityTypeCourse.semester.substr(this.activityTypeCourse.semester.length - 1, 1);

          if(!parseInt(this.lastIndex)){
            if(this.lastIndex != "0")
              this.activityTypeCourse.semester = this.activityTypeCourse.semester.substr(0, this.activityTypeCourse.semester.length - 1);
          } else if (this.activityTypeCourse.semester.length <= 4 && parseInt(this.activityTypeCourse.semester)) {
            this.activityTypeCourse.semester = String(parseInt(this.activityTypeCourse.semester));
          } else if (this.activityTypeCourse.semester.length > 5 && parseInt(this.lastIndex) ) {
            this.activityTypeCourse.semester = this.activityTypeCourse.semester.substr(0, 6);
          }
        }
        break;
      case "numberHourMax":
        this.activityTypeCourse.numberHourMax = parseInt(this.activityTypeCourse.numberHourMax + "");
        break;
      case "numberHourByOccurrence":
        this.activityTypeCourse.numberHourByOccurrence = parseInt(this.activityTypeCourse.numberHourByOccurrence + "");
        break;
      case "numberOccurrenceMax":
        this.activityTypeCourse.numberOccurrenceMax = parseInt(this.activityTypeCourse.numberOccurrenceMax + "");
        break;
      case "numberHourRelationship":
        this.activityTypeCourse.numberHourRelationship = parseInt(this.activityTypeCourse.numberHourRelationship + "");
        break;
    }
  }
}
