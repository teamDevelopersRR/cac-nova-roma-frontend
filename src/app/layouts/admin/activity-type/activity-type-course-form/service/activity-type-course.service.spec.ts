/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ActivityTypeCourseService } from './activity-type-course.service';

describe('Service: ActivityTypeCourse', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActivityTypeCourseService]
    });
  });

  it('should ...', inject([ActivityTypeCourseService], (service: ActivityTypeCourseService) => {
    expect(service).toBeTruthy();
  }));
});