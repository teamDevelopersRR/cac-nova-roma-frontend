import { ActivityTypeCourse } from './../model/activity-type-course.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../../../environments/environment';

@Injectable()
export class ActivityTypeCourseService {

    constructor(private http: HttpClient) { }

    async insert(activityTypeCourse: ActivityTypeCourse) {
      return await this.http.post<ActivityTypeCourse>(`${environment.url_application}/activityTypeCourse/`, activityTypeCourse).toPromise();
    }
  
    async update(activityTypeCourse: ActivityTypeCourse) {
      return await this.http.put<ActivityTypeCourse>(`${environment.url_application}/activityTypeCourse/`, activityTypeCourse).toPromise();
    }
  
    async delete(activityTypeCourse: ActivityTypeCourse) {
      return await this.http.delete<ActivityTypeCourse>(`${environment.url_application}/activityTypeCourse/${activityTypeCourse.id}`).toPromise();
    }
  
    async getAll() {
      return await this.http.get<ActivityTypeCourse[]>(`${environment.url_application}/activityTypeCourse/`).toPromise();
    }
  
    async findById(id: number) {
      return await this.http.get<ActivityTypeCourse>(`${environment.url_application}/activityTypeCourse/${id}`).toPromise();
    }
  
    async findByActivityType(id: number) {
      return await this.http.get<ActivityTypeCourse[]>(`${environment.url_application}/activityTypeCourse/getByActivityType/${id}`).toPromise();
    }
  
}