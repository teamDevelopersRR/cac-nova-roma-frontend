import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {} from 'jasmine';

import { ActivityTypeCourseFormComponent } from './activity-type-course-form.component';

describe('ActivityTypeCourseFormComponent', () => {
  let component: ActivityTypeCourseFormComponent;
  let fixture: ComponentFixture<ActivityTypeCourseFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityTypeCourseFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityTypeCourseFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
