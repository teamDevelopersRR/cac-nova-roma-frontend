import { ActivityType } from "../../model/activity-type.model";
import { Course } from "../../../course/model/course.model";
import { Basic } from "../../../models/basic.model";

export class ActivityTypeCourse extends Basic {
    semester: string;
    type: string;
    numberHourMax: number;
    numberHourRelationship: number;
    numberHourByOccurrence: number;
    numberOccurrenceMax: number;
    activityType: ActivityType = new ActivityType();
    course: Course = new Course();
}
