import { ProfilePermission } from './../model/profile-permission.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../../../environments/environment';

@Injectable()
export class ProfilePermissionService {

  constructor(private http: HttpClient) { }

  async insert(profilePermission: ProfilePermission) {
    return await this.http.post<ProfilePermission>(`${environment.url_application}/profilePermission/`, profilePermission).toPromise();
  }

  async update(profilePermission: ProfilePermission) {
    return await this.http.put<ProfilePermission>(`${environment.url_application}/profilePermission/`, profilePermission).toPromise();
  }
  
  async delete(profilePermission: ProfilePermission) {
    return await this.http.delete<ProfilePermission>(`${environment.url_application}/profilePermission/${profilePermission.id}`).toPromise();
  }

  async getAll() {
    return await this.http.get<ProfilePermission[]>(`${environment.url_application}/profilePermission/`).toPromise();
  }

  async findById(id: number) {
    return await this.http.get<ProfilePermission>(`${environment.url_application}/profilePermission/${id}`).toPromise();
  }

  async findByProfile(id: number) {
    return await this.http.get<ProfilePermission[]>(`${environment.url_application}/profilePermission/getByProfile/${id}`).toPromise();
  }

}
