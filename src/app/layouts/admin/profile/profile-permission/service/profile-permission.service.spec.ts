import { TestBed, inject } from '@angular/core/testing';

import { ProfilePermissionService } from './profile-permission.service';

describe('ProfilePermissionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProfilePermissionService]
    });
  });

  it('should be created', inject([ProfilePermissionService], (service: ProfilePermissionService) => {
    expect(service).toBeTruthy();
  }));
});
