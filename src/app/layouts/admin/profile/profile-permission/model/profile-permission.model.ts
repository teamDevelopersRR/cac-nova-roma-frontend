import { Profile } from './../../../profile/model/profile.model';
import { Permission } from './../../../permission/model/permission.model';
import { Basic } from '../../../models/basic.model';

export class ProfilePermission extends Basic {
    insert: boolean;
    read: boolean;
    update: boolean;
    delete: boolean;
    profile?: Profile = new Profile();
    permission: Permission = new Permission();
}
