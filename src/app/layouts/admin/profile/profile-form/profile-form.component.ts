import { ProfilePermissionDto } from './../model/profile-permission-dto.model';
import { ErrorValidationService } from './../../../../common/shared/externals/forms/field-error/service/error-validation.service';
import { Profile } from './../model/profile.model';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { IOption } from '../../../../common/shared/externals/forms/form.component';
import { ProfileService } from '../service/profile.service';
import { ProfilePermission } from '../profile-permission/model/profile-permission.model';
import { ProfilePermissionService } from '../profile-permission/service/profile-permission.service';
import { ProfileTypeService } from '../../profile-type/service/profile-type.service';
import { AlertCenterService } from '../../../../common/shared/components/alert-center/service/alert-center.service';
import { TITLE_ERROR, MESSAGE_OPERATION_ERROR, TITLE_SUCCESS, MESSAGE_OPERATION_SUCCESS } from '../../../../util/ConstantProject';
import { FormList } from '../../form-list';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.scss']
})
export class ProfileFormComponent extends FormList implements OnInit {

  constructor(public translate: TranslateService,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public errorValidationService: ErrorValidationService,
    public alertCenterService: AlertCenterService,
    public profileService: ProfileService,
    public profileTypeService: ProfileTypeService,
    public profilePermissionService: ProfilePermissionService,
    public ngxSmartModalService: NgxSmartModalService) {
    super(translate, router, activeRoute, 'profile', errorValidationService, alertCenterService, ngxSmartModalService);
  }

  profile: Profile = new Profile();

  dataProvider: ProfilePermissionDto[] = [];
  selectedProfilePermission: ProfilePermission = new ProfilePermission();

  listProfileType = [];
  listHierarchy: IOption[] = [
    {
      id: 1,
      label: '1',
      hasLabel: true,
      value: 1
    },
    {
      id: 2,
      label: '2',
      hasLabel: true,
      value: 2
    },
    {
      id: 3,
      label: '3',
      hasLabel: true,
      value: 3
    }
  ];

  ngOnInit() {
    this.checkEdit();
    this.loadListsProfiles();
  }

  preparateObject() {
    this.isLoading = true;
    this.profile.profilePermissions = <ProfilePermission[]>this.transformProfilePermissions();
    const copy = Object.assign({}, this.profile);
    return copy;
  }

  insert(): void {
    this.profileService.insert(this.preparateObject()).then(
      profile => {
        if ((<any>profile).exception)
          this.errorValidation(profile);
        else this.callbackInsertOrUpdate();
      }, (response: Response) => {
        this.errorValidation(response);
      }
    );
  }

  update(): void {
    this.profileService.update(this.preparateObject()).then(
      profile => {
        if ((<any>profile).exception)
          this.errorValidation(profile);
        else this.callbackInsertOrUpdate();
      }, (response: Response) => {
        this.errorValidation(response);
      }
    );
  }

  async getProfilePermissionByProfile() {
    this.isLoaded = false;
    await this.profileService.getProfilePermissionsByProfile(this.profile.id).then((result: any[]) => {
      this.dataProvider = result;
    });
    this.isLoaded = true;
  }

  findById(id: any) {
    return this.profileService.findById(id).then(
      profile => {
        this.profile = profile;

        this.getProfilePermissionByProfile();
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
        console.log(error);
      }
    );
  }

  async loadListsProfiles() {
    this.isLoaded = false;
    
    await this.profileService.getProfileTypes().then(
      profileTypes => {
        this.listProfileType = this.modelListToIOptions(profileTypes, 'name');
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
        console.log(error);
      }
    );

    await this.profileService.getProfilePermissions().then((result: any[]) => {
      this.dataProvider = result;
    });
    
    this.isLoaded = true;
  }

  confirmDelete(item) {
    this.profilePermissionService.delete(item).then(
      item => {
        this.getProfilePermissionByProfile();
        this.alertCenterService.alertSuccess(TITLE_SUCCESS, MESSAGE_OPERATION_SUCCESS);
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
        console.log(error);
      }
    );
  }

  openModalDetails(item) {
    this.selectedProfilePermission = item;
    this.selectedProfilePermission.profile = this.profile;
    this.openModal();
  }

  transformProfilePermissions() {
    return this.dataProvider
      .filter((pp: ProfilePermissionDto) => pp.read)
      .map((pp: ProfilePermissionDto) => {
        return {
          permission: {
            id: pp.permission.id,
            crud: pp.permission.crud
          },
          read: pp.permission.crud,
          insert: pp.insert,
          update: pp.update,
          delete: pp.delete
        }
      });
  }

  changePermission(item: ProfilePermissionDto, type) {
    switch (type) {
      case 'insert':
        item.insert = !item.insert;
        break;
        case 'update':
        item.update = !item.update;
        break;
        case 'delete':
        item.delete = !item.delete;
        break;
      default:
        break;
    }
    item.read = item.read || (item.insert || item.delete || item.update);
  }

  disabledRead(pp: ProfilePermissionDto) {
    return pp.disabledRead || (pp.insert || pp.delete || pp.update);
  }
}
