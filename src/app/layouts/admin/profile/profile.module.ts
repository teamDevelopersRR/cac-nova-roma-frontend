import { RouterModule } from '@angular/router';
import { AppCommonModule } from './../../../common/app-common.module';
import { NgModule } from '@angular/core';
import { ProfileFormComponent } from './profile-form/profile-form.component';
import { ProfileListComponent } from './profile-list/profile-list.component';
import { profileRoutes } from './profile.routing';
import { ProfileService } from './service/profile.service';
import { ProfilePermissionService } from './profile-permission/service/profile-permission.service';
import { PermissionService } from '../permission/service/permission.service';
import { ProfileTypeService } from '../profile-type/service/profile-type.service';
import { NgxSmartModalModule } from 'ngx-smart-modal';

@NgModule({
  imports: [
    AppCommonModule,
    RouterModule.forChild(profileRoutes),
    NgxSmartModalModule.forRoot()
  ],
  declarations: [
    ProfileFormComponent,
    ProfileListComponent
  ],
  providers: [ProfileService, 
    ProfilePermissionService, 
    PermissionService,
    ProfileTypeService]
})
export class ProfileModule { }