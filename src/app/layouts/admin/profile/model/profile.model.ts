import { ProfilePermission } from "../profile-permission/model/profile-permission.model";
import { Basic } from "../../models/basic.model";
import { ProfileType } from "../../profile-type/model/profile-type.model";

export class Profile extends Basic {
    name: string;
    hierarchy: number;
    type: ProfileType = new ProfileType();
    profilePermissions: Array<ProfilePermission>;
}