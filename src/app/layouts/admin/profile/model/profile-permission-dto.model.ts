export class ProfilePermissionDto {
    permission: {
        id: number,
        crud: boolean
    };
    disabledRead: boolean;
    disabledInsert: boolean;
    disabledUpdate: boolean;
    disabledDelete: boolean;
    read: boolean;
    insert: boolean;
    update: boolean;
    delete: boolean;
}