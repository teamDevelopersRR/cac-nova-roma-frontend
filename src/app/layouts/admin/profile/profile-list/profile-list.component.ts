import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../service/profile.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { List } from '../../list';
import { AlertCenterService } from '../../../../common/shared/components/alert-center/service/alert-center.service';
import { TITLE_ERROR, MESSAGE_OPERATION_ERROR, TITLE_SUCCESS, MESSAGE_OPERATION_SUCCESS } from '../../../../util/ConstantProject';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Profile } from '../model/profile.model';

@Component({
  selector: 'app-profile-list',
  templateUrl: './profile-list.component.html',
  styleUrls: ['./profile-list.component.scss']
})
export class ProfileListComponent extends List implements OnInit {

  dataProvider = [];
  selectedProfile: Profile = new Profile();

  constructor(public translate: TranslateService,
    public router: Router,
    public activeRoute: ActivatedRoute,
    private profileService: ProfileService,
    public alertCenterService:AlertCenterService,
    public ngxSmartModalService: NgxSmartModalService) {
    super(translate, router, activeRoute, "profile", alertCenterService, ngxSmartModalService);
  }

  ngOnInit() {
    this.loadDataProvider();
  }

  async loadDataProvider() {
    this.dataProvider = [];
    await this.profileService.getAll().then((item) => this.dataProvider.push(item));
    this.isLoaded = true;
  }

  edit(item){
    this.router.navigate([this.routeEdit+item.id]);
  }

  confirmDelete(item){
    this.profileService.delete(item).then(
      item => {
        this.loadDataProvider();
        this.alertCenterService.alertSuccess(TITLE_SUCCESS, MESSAGE_OPERATION_SUCCESS);
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
        console.log(error);
      }
    );
  }

  openModalDetails(item){
    this.selectedProfile = item;
    this.openModal();
  }

}
