import { ProfileType } from './../../profile-type/model/profile-type.model';
import { ProfilePermissionDto } from './../model/profile-permission-dto.model';
import { Profile } from './../model/profile.model';
import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AuthenticationService } from '../../../../common/authentication/authentication.service';
import { LoggedUser } from '../../../../common/authentication/logged-user';

@Injectable()
export class ProfileService {

  constructor(private http: HttpClient,
    private authenticationService: AuthenticationService) { }

  async insert(profile: Profile) {
    return await this.http.post<Profile>(`${environment.url_application}/profile/`, profile).toPromise();
  }

  async update(profile: Profile) {
    return await this.http.put<Profile>(`${environment.url_application}/profile/`, profile).toPromise();
  }

  async delete(profile: Profile) {
    return await this.http.delete<Profile>(`${environment.url_application}/profile/${profile.id}`).toPromise();
  }

  async getAll() {
    return await this.http.get<Profile[]>(`${environment.url_application}/profile/`).toPromise();
  }

  async findById(id: number) {
    return await this.http.get<Profile>(`${environment.url_application}/profile/${id}`).toPromise();
  }

  async findProfileByUserLogged() {
    let loggedUser: LoggedUser = this.authenticationService.getLoggedUser();
    return await this.http.get<Profile>(`${environment.url_application}/profile/${loggedUser.profile.id}`).toPromise();
  }

  async findByName(name) {
    const params = new HttpParams().set('name', name);

    return await this.http.post<Profile[]>(`${environment.url_application}/profile/getByName?`, null,
      {
        params: params,
        observe: 'response'
      }
    ).toPromise();
  }

  async getProfilePermissionsByProfile(profileId: number): Promise<ProfilePermissionDto[]> {
    return await this.http.get<ProfilePermissionDto[]>(`${environment.url_application}/profile/permissions/${profileId}`).toPromise();
  }

  async getProfilePermissions(): Promise<ProfilePermissionDto[]> {
    return await this.http.get<ProfilePermissionDto[]>(`${environment.url_application}/profile/permissions/`).toPromise();
  }

  async getProfileTypes(): Promise<ProfileType[]> {
    return await this.http.get<ProfileType[]>(`${environment.url_application}/profile/profileTypes/`).toPromise();
  }

}
