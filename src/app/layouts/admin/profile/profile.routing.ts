import { AuthGuard } from './../../../common/guards/auth.guard';
import { ProfileListComponent } from './profile-list/profile-list.component';
import { ProfileFormComponent } from './profile-form/profile-form.component';
import { Routes } from '@angular/router';

export const profileRoutes: Routes = [
    {
        path: '',
        component: ProfileListComponent,
        data: {
            breadcrumb: 'breadcrumb.profiles',
            permissions: ['ROLE_PROFILE_READ']
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'edit/:id',
        component: ProfileFormComponent, 
        data: {
            breadcrumb: 'breadcrumb.profileEdit',
            permissions: ['ROLE_PROFILE_UPDATE']
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'new',
        component: ProfileFormComponent, 
        data: {
            breadcrumb: 'breadcrumb.profileNew',
            permissions: ['ROLE_PROFILE_INSERT']
        },
        canActivate: [AuthGuard]
    }
];