import { AdminLayoutComponent } from './admin-layout.component';
import { Routes } from '@angular/router';

export const AdminLayoutRoutes: Routes = [
    {
        path: '', component: AdminLayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'help',
                pathMatch: 'full'
            }, {
                path: 'help',
                loadChildren: './help/help.module#HelpModule'
            }, {
                path: 'location-college',
                loadChildren: './location-college/location-college.module#LocationCollegeModule'
            },
            {
                path: 'activity',
                loadChildren: './activity/activity.module#ActivityModule'
            },
            {
                path: 'activity-type',
                loadChildren: './activity-type/activity-type.module#ActivityTypeModule'
            },
            {
                path: 'course',
                loadChildren: './course/course.module#CourseModule'
            },
            {
                path: 'discipline',
                loadChildren: './discipline/discipline.module#DisciplineModule'
            },
            {
                path: 'profile',
                loadChildren: './profile/profile.module#ProfileModule'
            },
            {
                path: 'profile-type',
                loadChildren: './profile-type/profile-type.module#ProfileTypeModule'
            },
            {
                path: 'users',
                loadChildren: './users/users.module#UsersModule'
            },
            {
                path: 'permission',
                loadChildren: './permission/permission.module#PermissionModule'
            }
        ]
    }
];
