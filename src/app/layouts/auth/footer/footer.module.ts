import { NgModule } from '@angular/core';
import { FooterComponent } from './footer.component';
import { AppCommonModule } from '../../../common/app-common.module';

@NgModule({
  imports: [
    AppCommonModule
  ],
  exports: [FooterComponent],
  declarations: [FooterComponent]
})
export class FooterModule { }
