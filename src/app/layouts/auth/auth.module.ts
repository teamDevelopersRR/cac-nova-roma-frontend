import { AppCommonModule } from './../../common/app-common.module';
import { AuthComponent } from './auth.component';
import { LockScreenComponent } from './lock-screen/lock-screen.component';
import { ForgotComponent } from './forgot/forgot.component';
import { AuthRoutes } from './auth.routing';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FooterModule } from './footer/footer.module';
import { AlertCenterModule } from '../../common/shared/components/alert-center/alert-center.module';

@NgModule({
  imports: [
    AppCommonModule,
    RouterModule.forChild(AuthRoutes),
    FormsModule,
    ReactiveFormsModule,
    FooterModule
  ],
  declarations: [AuthComponent, ForgotComponent, LockScreenComponent]
})

export class AuthModule {}

