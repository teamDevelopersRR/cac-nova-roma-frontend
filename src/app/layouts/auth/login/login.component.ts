import { Credential } from "./credential.model";
import { AuthenticationService } from "./../../../common/authentication/authentication.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Component } from "@angular/core";
import { AlertCenterService } from "../../../common/shared/components/alert-center/service/alert-center.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent {
  credential: Credential = new Credential();
  isShowPassword: boolean = false;
  isLoaderAuthentication = false;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthenticationService,
    private alertCenterService: AlertCenterService
  ) {}

  async onAuthenticate(validForm) {
    if (validForm) {
      this.isLoaderAuthentication = true;

      await this.authService
        .authentication(this.credential.login, this.credential.password)
        .then(resp => {
          this.isLoaderAuthentication = false;
          resp.body.token = resp.headers.get("x-auth-token");
          this.authService.setLoggedUser(resp.body);
          this.alertCenterService.alertSuccess(
            "alert.welcome",
            this.authService.getLoggedUser().name,
            false
          );
          this.router.navigate(["/"]);
        })
        .catch(error => {
          this.isLoaderAuthentication = false;
          //console.error('[' + error.error + '] - ' + error.message);
        });
    }
  }

  showPassword() {
    this.isShowPassword = !this.isShowPassword;
  }
}
