import { AppCommonModule } from './../../../common/app-common.module';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoginRoutes } from './login.routing';
import { LoginComponent } from './login.component';
import { FooterModule } from '../footer/footer.module';

@NgModule({
    imports: [
        AppCommonModule,
        RouterModule.forChild(LoginRoutes),
        FooterModule
    ],
    declarations: [LoginComponent]
})

export class LoginModule { }
