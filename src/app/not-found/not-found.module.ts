import { NotFoundComponent } from './not-found.component';
import { NotFoundRoutes } from './not-found.routing';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(NotFoundRoutes),
  ],
  declarations: [NotFoundComponent]
})

export class NotFoundModule {}
